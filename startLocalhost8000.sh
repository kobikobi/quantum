cd dist
echo -e "Serving Quantum at local network http://"`hostname -I`"\b:8000"
echo -e "To open WSL to local network, try https://stackoverflow.com/a/66485783/7586"
python -m http.server 8000
