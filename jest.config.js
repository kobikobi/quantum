module.exports = {
  verbose: true,
  transform: {
    '^.+\\.js$': 'babel-jest',
    // "^.+\\.(css|scss|less)$": "jest-css-modules"
  },
  setupFilesAfterEnv: ['./src/tests/setupTests.js'],
  // globals: {
  //   NODE_ENV: "test"
  // },
  moduleFileExtensions: [
    'js',
    'jsx',
  ],
  moduleDirectories: [
    'node_modules',
    'src/frontend',
    'src/shared',
  ],
  moduleNameMapper: {
    '\\.(?:jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$': '<rootDir>/src/tests/mocks/staticFileMock.js',
    '\\.(?:css|less|sass|scss)$': '<rootDir>/src/tests/mocks/styleMock.js',
  },

};
