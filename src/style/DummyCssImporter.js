// this is used only for webpack, because WeBpAcK dOeS nOt UnDeRsTaNd CsS
import './main.scss';
import './animation.scss';
import './tileSizes.scss';
import './backgrounds.scss';
import './score.scss';
import './tileColors.scss';
import './lightmode.scss';
import './tutorial.scss';
