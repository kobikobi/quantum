import Picture from './Picture';
import { TileStates } from './Constants';

class Board {
  constructor(tiles) {
    this.tiles = tiles;
    // this.knownTiles = new Set();
    // this.knownTilesByPicture = new Map();
  }

  getKnownTiles(picture) {
    if (picture instanceof Picture) {
      // this can be more efficient if we're using a Map, knownTilesByPicture.
      return this.tiles.filter(t => t.picture === picture);
    }

    return this.tiles.filter(t => t.state === TileStates.revealed);
  }

  drawBoard() {
    return this.tiles
      .map((t, i) => `${i} ${t}`)
      .join('\n');
  }
}

export default Board;
