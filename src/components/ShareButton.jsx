import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import ShareIcon from '@material-ui/icons/Share';
import { withStyles } from '@material-ui/core/styles';
import Snackbar from '@material-ui/core/Snackbar';

const styles = theme => ({
  leftIcon: {
    marginRight: theme.spacing(1),
  },
});

async function fallbackCopyText(shareData) {
  const clipboardText = `${shareData.title}\n${shareData.text || ''} ${shareData.url}`;
  await navigator.clipboard.writeText(clipboardText);
}

/**
 *
 * @param {*} gameScore set in App.jsx, setGameScore, example:
 * { current:int, best:int, newPersonalRecord:bool, matchesPersonalRecord:bool }
 * @returns
 */
function buildShareText(gameScore) {
  let prefix = 'Try';
  if (gameScore && gameScore.current > 0) {
    prefix = `I scored ${gameScore.current} in`;
    if (gameScore.newPersonalRecord) {
      prefix = `New record! ${prefix}`;
    }
  }
  return `${prefix} Quantum, the infinite memory game!`;
}

async function onShareClick(gameScore, showCopiedConfirmationCallback) {
  const shareData = {
    title: buildShareText(gameScore),
    // text: "sharing text" // not used, title is enough
    // eslint-disable-next-line no-restricted-globals
    url: location.href,
  };
  try {
    await navigator.share(shareData);
  } catch (err) {
    // if the user aborted the share, don't copy
    if (err instanceof DOMException && err.com === DOMException.ABORT_ERR) return;
    try {
      await fallbackCopyText(shareData);
      showCopiedConfirmationCallback();
    } catch (deepErr) {
      console.error(`failed copying share text to clipboard\nshare text was:\n${shareData.title}\n${shareData.text}`, deepErr);
    }
  }
}

function ShareButton({ gameScore, classes }) {
  const [showCopiedToast, setShowCopiedToast] = useState(false);

  return (
    <>
      <Button
        variant="outlined"
        color="secondary"
        className="ShareButton"
        onClick={() => onShareClick(gameScore, () => setShowCopiedToast(true))}
      >
        <ShareIcon className={classes.leftIcon} />
        Share
      </Button>
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        open={showCopiedToast}
        autoHideDuration={600}
        onClose={() => setShowCopiedToast(false)}
        message="Copied!"
      />
    </>
  );
}

export default withStyles(styles)(ShareButton);
