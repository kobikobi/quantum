import React from 'react';
import Button from '@material-ui/core/Button';
import classNames from 'classnames';
import { Layouts, LayoutNames } from './Layouts';
import LayoutPreview from './LayoutPreview.jsx';

export default function LayoutPicker({ currentLayoutName, changeUiSetting }) {
  return (
    <div className="LayoutPicker">
      {LayoutNames.map(layoutName => {
        const selectedLayout = currentLayoutName === layoutName;
        const buttonStyle = { borderRadius: 0 };
        return (
          <Button
            radioGroup="x"
            width={64} // variant="contained" color="primary"
            key={`LayoutPreviewButton${layoutName}`}
            style={buttonStyle}
            className={classNames('BigButtonCheckbox', { Selected: selectedLayout })}
            onClick={() => changeUiSetting(ui => { ui.layoutName = layoutName; })}
          >
            <LayoutPreview layout={Layouts.get(layoutName)} key={`LayoutPreview${layoutName}`} />
          </Button>
        );
      })}
    </div>
  );
}
