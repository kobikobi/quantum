import React from 'react';
import { useTheme } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import NewGameIcon from '@material-ui/icons/ViewModule'; // ViewComfy
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';

export default function SettingsMenuNewGameButton({ uiSettingsWillChangeAtNextGame, onClickNewGame }) {
  const theme = useTheme();
  const primaryColor = theme.palette.primary.main;
  // const menuBackgroundColor = theme.palette.background.default;
  const menuTextColor = theme.palette.text.primary;
  const primaryButtonTextColor = theme.palette.primary.contrastText;

  return (
    <MenuItem
      button
      onClick={() => { onClickNewGame(); }}
      style={{
        backgroundColor: uiSettingsWillChangeAtNextGame ? primaryColor : undefined,
        minHeight: '3.0rem',
      }}
    >
      <ListItemIcon>
        <NewGameIcon
          style={{ color: uiSettingsWillChangeAtNextGame ? primaryButtonTextColor : menuTextColor }}
        />
      </ListItemIcon>
      <ListItemText
        disableTypography
        primary={(
          <Typography
            variant="body2"
            style={{
              color: uiSettingsWillChangeAtNextGame
                ? primaryButtonTextColor : menuTextColor,
              lineHeight: 1,
            }}
          >
            New Game
            <br />
            {
              uiSettingsWillChangeAtNextGame
                ? <small style={{ fontWeight: 'normal' }}>Settings will change for next game</small>
                : undefined
            }
          </Typography>
        )}
      />
    </MenuItem>
  );
}
