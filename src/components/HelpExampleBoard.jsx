import React, { useMemo, useState, useEffect } from 'react';
import LayoutPreviewBase from './LayoutPreviewBase.jsx';
import { TileStates } from '../Constants';
import TileCard from './TileCard.jsx';
import pickRandomPictures from '../services/GameRandomPicturePicker';

export default function HelpExampleBoard({
  layout, tilesetName, frames, playAnimation,
}) {
  const pictures = useMemo(() => pickRandomPictures(tilesetName, 6), [pickRandomPictures]);
  const [currentFrameIndex, setCurrentFrameIndex] = useState(0);
  const currentFrame = frames[currentFrameIndex];
  const animationSlowdownFactor = 1.3;
  useEffect(
    () => {
      if (!playAnimation) return;
      const frameTimeout = currentFrame[0].timeout || 900;
      const timeoutId = setTimeout(
        () => setCurrentFrameIndex((currentFrameIndex + 1) % frames.length),
        frameTimeout * animationSlowdownFactor,
      );
      return () => { clearTimeout(timeoutId); };
    },
    [currentFrame, playAnimation],
  );
  const frameMessage = currentFrame[0].message;

  function renderTileCard(layoutTile) {
    const { tileIndex } = layoutTile;
    const currentTile = currentFrame[tileIndex] || {};
    const picture = currentTile.tilePicture ? pictures[currentTile.tilePicture] : undefined;

    return (
      <TileCard
        tileState={picture ? TileStates.revealed : TileStates.unknown}
        tilePicture={picture}
        key={`TutorialTile${tileIndex}`}
        onClickTile={() => { }}
        isCurrentTurnSelected={currentTile.isCurrentTurnSelected}
        isFailReason={currentTile.isFailReason}
        isMatched={currentTile.foundMatchCurrentTurn}
        tilesetName={tilesetName}
        debugMode={false}
      />
    );
  }

  return (
    <div className="TutorialBoardWrapper">
      <LayoutPreviewBase
        tilesElementUniqueKey="exampleBoard"
        tilesElementClass={`Tutorial Tiles Tileset${tilesetName}`}
        renderTileCard={renderTileCard}
        layout={layout}
        forceNoRotation
      />
      {frameMessage && <div className="Message">{frameMessage}</div>}
    </div>
  );
}
