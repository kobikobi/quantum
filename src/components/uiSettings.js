import { LayoutNames } from './Layouts';
import { TilesetNames } from './Tilesets';

const settingsName = 'settings';
const defaultTilesetName = 'Abstract';
const defaultLayoutName = 'Fifteen';

function saveSettings(uiSettings) {
  localStorage.setItem(settingsName, JSON.stringify(uiSettings));
}

export function ensureTilesetExists(loadedTilesetName) {
  return TilesetNames.includes(loadedTilesetName) ? loadedTilesetName : defaultTilesetName;
}
export function ensureLayoutNameExists(loadedLayoutName) {
  return LayoutNames.includes(loadedLayoutName) ? loadedLayoutName : defaultLayoutName;
}

export class UiSettings {
  constructor() {
    this.casualMode = false; // this isn't really a UI setting but it doesn't matter.
    this.tileset = defaultTilesetName;
    this.debugMode = false;
    this.markUnknownTiles = true;
    this.layoutName = defaultLayoutName;
  }
}

/**
 * Load UI Settings from local storage. Returns undefined if settings are not stored.
 * Unite loaded settings with default settings when missing.
 */
function loadSettings() {
  const serializedSavedSetting = localStorage.getItem(settingsName);
  if (!serializedSavedSetting) return undefined;
  const savedSetting = JSON.parse(serializedSavedSetting);
  const result = new UiSettings(); // load default settings
  Object.assign(result, savedSetting); // override default values with values from local storage.
  result.tileset = ensureTilesetExists(result.tileset);
  result.layoutName = ensureLayoutNameExists(result.layoutName);
  result.debugMode = false; // debugMode will be removed. If it is saved as true, set it to false.
  // todo: remove outdated properties that are saved in local storage but are no longer needed.
  return result;
}

export const UiSettingsLocalStorageService = { load: loadSettings, save: saveSettings };
