const layoutTileTypes = Object.freeze({
  Tile: 'Tile',
  Space: 'Space',
  HalfSpace: 'HalfSpace',
  Newline: 'Newline',
});

function parseLayout(lineArray) {
  if (typeof lineArray === 'string') {
    lineArray = lineArray.replace(/^(?: +\n)+/, '').split('\n');
  }
  const tiles = [];
  let tileCounter = 0;
  tiles.tileHeight = lineArray.length;
  tiles.tileWidth = 0;
  for (let lineIndex = 0; lineIndex < lineArray.length; lineIndex++) {
    if (lineIndex > 0) {
      tiles.push({
        type: layoutTileTypes.Newline,
        uniqueKey: `layoutNewline${lineIndex}`,
      });
    }
    const characters = lineArray[lineIndex].split('');
    let currentLineWidth = 0;
    for (let charIndex = 0; charIndex < characters.length; charIndex++) {
      switch (characters[charIndex]) {
        case 'x':
          tiles.push({
            lineIndex,
            columnIndex: charIndex,
            tileIndex: tileCounter,
            uniqueKey: `layoutTile${tileCounter}`,
            type: layoutTileTypes.Tile,
          });
          tileCounter++;
          currentLineWidth++;
          break;
        case ' ':
          tiles.push({
            type: layoutTileTypes.Space,
            uniqueKey: `layoutSpace${lineIndex}x${charIndex}`,
          });
          currentLineWidth++;
          break;
        case ',':
          tiles.push({
            type: layoutTileTypes.HalfSpace,
            uniqueKey: `layoutHalfSpace${lineIndex}x${charIndex}`,
          });
          currentLineWidth += 0.5;
          break;
      }
      tiles.tileWidth = Math.max(tiles.tileWidth, Math.ceil(currentLineWidth));
    }
  }
  return tiles;
}

const layouts = [
  {
    name: 'Six',
    hidden: true,
    difficulty: 'easy',
    pictureCount: 4,
    layout: parseLayout(
      [
        'xxx',
        'xxx',
      ],
    ),
  },
  {
    name: 'Nine',
    difficulty: 'easy',
    pictureCount: 6,
    layout: parseLayout(
      [
        'xxx',
        'xxx',
        'xxx'
      ],
    ),
  },
  {
    name: 'Thirteen',
    difficulty: 'easy',
    pictureCount: 8,
    layout: parseLayout(
      [
        'xxxx',
        'xxxxx',
        'xxxx',
      ],
    ),
  },
  {
    name: 'Fifteen',
    difficulty: 'medium',
    pictureCount: 9,
    layout: parseLayout(
      [
        'xxxxx',
        'xxxxx',
        'xxxxx',
      ],
    ),
  },
  // {
  //     name:"Sixteen",
  //     difficulty: 'medium',
  //     pictureCount:11,
  //     layout:parseLayout(
  //         [' ,xxxx',
  //          ' xxxx',
  //          ',xxxx',
  //          'xxxx',]
  //     ),
  // },
  // {
  //     name:"Hash",
  //     difficulty: 'hard',
  //     pictureCount: 9,
  //     layout:parseLayout(
  //          [
  //           'x x',
  //          'xxxxx',
  //           'x x',
  //          'xxxxx',
  //           'x x',
  //       ]),
  // },
  // {
  //     name:"Boxes",
  //     difficulty: 'medium',
  //     pictureCount: 11,
  //     layout:parseLayout(
  //         [
  //         'xx,xx',
  //         'xx,xx',
  //           ',',
  //         'xx,xx',
  //         'xx,xx',
  //         ]),
  // },
  {
    name: 'Boxes',
    difficulty: 'medium',
    pictureCount: 11,
    layout: parseLayout(
      [
        ',', // extra line so the layout is square and won't rotate.
        'xx,xx',
        'xx,xx',
        'xx,xx',
        'xx,xx',
      ],
    ),
  },
  {
    name: 'Smudge',
    difficulty: 'hard',
    pictureCount: 11,
    layout: parseLayout(
      [
        'xxx',
        'xxxx',
        'xxxx',
        'xxxx',
        'xxx',
      ],
    ),
  },
  {
    name: 'Quantum',
    difficulty: 'hard',
    pictureCount: 11,
    layout: parseLayout(
      [
        'xx',
        'xx',
        ',',
        'xx,xx',
        'xx,xx',
      ],
    ),
  },
];

/**
 * Return a size factor for the layout: L, M, or S.
 * Currently the size is only used for CSS, so I'm not making an enum for sizes.
 */
function getSizeFactor(layout) {
  const size = Math.max(layout.width, layout.height);
  if (size >= 8) return 'L';
  if (size >= 5) return 'M';
  return 'S';
}

layouts.forEach(l => { l.tileCount = l.layout.filter(t => t.type === layoutTileTypes.Tile).length; });
layouts.forEach(l => { l.width = l.layout.tileWidth; });
layouts.forEach(l => { l.height = l.layout.tileHeight; });
layouts.forEach(l => { l.sizeFactor = getSizeFactor(l); });

const layoutMap = new Map(layouts.map(i => [i.name, i]));

export const Layouts = layoutMap;
export const LayoutNames = layouts.map(l => l.name);
export const TileTypes = layoutTileTypes;
// this is only used for tests.
export const _layoutParserHelper = parseLayout;
