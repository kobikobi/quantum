import React, { useEffect, useState } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import NextIcon from '@material-ui/icons/ChevronRight';
import PrevIcon from '@material-ui/icons/ChevronLeft';
import CloseIcon from '@material-ui/icons/Close';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Typography from '@material-ui/core/Typography';
import classNames from 'classnames';
import { Layouts } from './Layouts';
import HelpExampleBoard from './HelpExampleBoard.jsx';
import { TilesetNames } from './Tilesets';
import { intro, failSecondFlipNoMatch, failShouldSelectMatch } from './HelpExampleAnimations';

const sampleLayout = Layouts.get('Six');

const styles = theme => ({
  // button: {
  //   margin: theme.spacing(1),
  // },
  leftIcon: {
    marginRight: theme.spacing(1),
  },
  rightIcon: {
    marginLeft: theme.spacing(1),
  },
  footerLeftAlign: {
    marginRight: 'auto',
  },
  dialogTitle: {
    paddingBottom: 0,
  },
  switchWithoutMargin: {
    marginLeft: 0,
  },
  // iconSmall: {
  //   fontSize: 20,
  // },
});

function HelpDialog({
  classes, helpDialogVisible, hideHelpDialog, tilesetName, uiSettings, changeUiSetting, isNewGameAndNoTiles,
}) {
  const [currentFrame, setCurrentFrame] = useState(0);
  const maxFrames = 3; // zero-based indexed
  const isFirstFrame = currentFrame === 0;
  const isLastFrame = currentFrame === maxFrames;
  tilesetName = tilesetName || TilesetNames[0];
  // reset frame on re-open
  useEffect(() => {
    if (helpDialogVisible) setCurrentFrame(0);
  }, [helpDialogVisible, setCurrentFrame]);

  return (
    <Dialog
      open={helpDialogVisible}
      onClose={hideHelpDialog}
      aria-labelledby="responsive-dialog-title"
    >
      <DialogTitle id="responsive-dialog-title" className={classes.dialogTitle}>How to play?</DialogTitle>
      <DialogContent>
        <div className="CarouselWrapper Help HelperMarkKnownTiles">
          <div className={classNames('CarouselScroller', `CarouseSelectedFrame${currentFrame}`)}>
            <div className="CarouselFrame">
              <Typography variant="body1">
                Quantum is an infinite memory game. Unlike other memory games, matched cards are replaced with new ones:
              </Typography>
              <HelpExampleBoard layout={sampleLayout} tilesetName={tilesetName} frames={intro} playAnimation={currentFrame === 0} />
            </div>
            <div className="CarouselFrame">
              <Typography variant="body1">
                The game ends if a card is flipped for the second time without finding its match:
              </Typography>
              <HelpExampleBoard layout={sampleLayout} tilesetName={tilesetName} frames={failSecondFlipNoMatch} playAnimation={currentFrame === 1} />
            </div>
            <div className="CarouselFrame">
              <Typography variant="body1">
                The game will also end if an existing matching card wasn't picked:
              </Typography>
              <HelpExampleBoard layout={sampleLayout} tilesetName={tilesetName} frames={failShouldSelectMatch} playAnimation={currentFrame === 2} />
            </div>
            <div className="CarouselFrame">
              <Typography variant="body1" gutterBottom>
                Not your kind of fun? We also have a catual true-infinite mode:
              </Typography>
              <FormControlLabel
                className={classes.switchWithoutMargin}
                control={(
                  <Switch
                    onChange={() => changeUiSetting(ui => { ui.casualMode = !ui.casualMode; })}
                    checked={uiSettings.casualMode}
                  />
                )}
                label="Casual Mode"
              />
              <Typography variant="body1">
                You can always switch back on the Options menu.
              </Typography>
            </div>
          </div>
        </div>
      </DialogContent>
      <DialogActions>
        {isFirstFrame
          ? (
            <Button
              onClick={() => hideHelpDialog()}
              color="default"
              autoFocus
              variant="text"
              className={classes.footerLeftAlign}
            >
              <CloseIcon />
              {isNewGameAndNoTiles ? 'Skip' : 'Back'}
            </Button>
          )
          : (
            <Button
              onClick={() => setCurrentFrame(currentFrame - 1)}
              color="default"
              autoFocus
              variant="text"
              disabled={isFirstFrame}
              className={classes.footerLeftAlign}
            >
              <PrevIcon />
              Previous
            </Button>
          )}
        {isLastFrame
          ? (
            <Button onClick={hideHelpDialog} color="primary" autoFocus variant="contained">
              {isNewGameAndNoTiles ? 'Start' : 'Close'}
            </Button>
          )
          : (
            <Button onClick={() => setCurrentFrame(currentFrame + 1)} color="primary" autoFocus variant="contained">
              Next
              <NextIcon />
            </Button>
          )}
      </DialogActions>
    </Dialog>
  );
}

export default withStyles(styles)(withMobileDialog()(HelpDialog));
