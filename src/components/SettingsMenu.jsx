import React from 'react';
import List from '@material-ui/core/List';
import Switch from '@material-ui/core/Switch';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import ListItem from '@material-ui/core/ListItem';
import SvgIcon from '@material-ui/core/SvgIcon';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import CasualModeIcon from '@material-ui/icons/LocalFlorist'; // Toys
import DebugModeIcon from '@material-ui/icons/PhotoFilterSharp';
import AboutIcon from '@material-ui/icons/AlternateEmail'; // PanoramaFishEye TripOrigin
import HelpIcon from '@material-ui/icons/Help';
import Divider from '@material-ui/core/Divider';
import MarkKnownTilesIcon from '../images/icons/MarkKnownCardsIcon.svg'; // TextureSharp FormatPaint
import LayoutPicker from './LayoutPicker.jsx';
import TilesetPicker from './TilesetPicker.jsx';
import SettingsMenuNewGameButton from './SettingsMenuNewGameButton.jsx';
import ShareButton from './ShareButton.jsx';

// eslint-disable-next-line no-restricted-globals
const isLocalHost = location.hostname === 'localhost';
const showDebugMode = isLocalHost;

export default function SettingsMenu({
  uiSettings,
  uiSettingsWillChangeAtNextGame,
  changeUiSetting,
  setSettingsDrawerVisible,
  showSettingsDrawer,
  onClickNewGame,
  gameIsCasualMode,
  showHelpDialog,
  showAboutDialog,
  gameScore,
}) {
  function closeMenu() {
    setSettingsDrawerVisible(false);
  }

  return (
    <SwipeableDrawer
      className="SettingsMenu"
      anchor="right"
      open={showSettingsDrawer}
      onClose={closeMenu}
      onOpen={() => setSettingsDrawerVisible(true)}
    >
      <List dense>
        <SettingsMenuNewGameButton
          onClickNewGame={() => { onClickNewGame(); closeMenu(); }}
          uiSettingsWillChangeAtNextGame={uiSettingsWillChangeAtNextGame}
        />
        <Divider />
        <LayoutPicker
          currentLayoutName={uiSettings.layoutName}
          changeUiSetting={changeUiSetting}
        />
        <Divider />
        <TilesetPicker currentTilesetName={uiSettings.tileset} changeUiSetting={changeUiSetting} />
        <Divider />
        <ListItem dense button onClick={() => changeUiSetting(ui => { ui.casualMode = !ui.casualMode; })}>
          <ListItemIcon>
            <CasualModeIcon color={uiSettings.casualMode ? 'secondary' : 'inherit'} />
          </ListItemIcon>
          <ListItemText
            primary="Casual"
            secondary={gameIsCasualMode === uiSettings.casualMode
              ? '' : `Next game will be ${gameIsCasualMode ? 'brutal' : 'casual'}`}
          />
          <ListItemSecondaryAction>
            <Switch
              disableRipple
              color="secondary"
              onClick={() => changeUiSetting(ui => { ui.casualMode = !ui.casualMode; })}
              checked={uiSettings.casualMode}
            />
          </ListItemSecondaryAction>
        </ListItem>
        <ListItem dense button onClick={() => changeUiSetting(ui => { ui.markUnknownTiles = !ui.markUnknownTiles; })}>
          <ListItemIcon>
            <SvgIcon color={uiSettings.markUnknownTiles ? 'secondary' : 'inherit'}>
              <MarkKnownTilesIcon />
            </SvgIcon>
          </ListItemIcon>
          <ListItemText primary="Mark known cards" />
          <ListItemSecondaryAction>
            <Switch
              disableRipple
              color="secondary"
              onClick={() => changeUiSetting(ui => { ui.markUnknownTiles = !ui.markUnknownTiles; })}
              checked={uiSettings.markUnknownTiles}
            />
          </ListItemSecondaryAction>
        </ListItem>
        {showDebugMode
          ? (
            <ListItem dense button onClick={() => changeUiSetting(ui => { ui.debugMode = !ui.debugMode; })}>
              <ListItemIcon>
                <DebugModeIcon color={uiSettings.debugMode ? 'secondary' : 'inherit'} />
              </ListItemIcon>
              <ListItemText primary="Debug Mode" />
              <ListItemSecondaryAction>
                <Switch
                  disableRipple
                  color="secondary"
                  onClick={() => changeUiSetting(ui => { ui.debugMode = !ui.debugMode; })}
                  checked={uiSettings.debugMode}
                />
              </ListItemSecondaryAction>
            </ListItem>
          )
          : null}
        <Divider />
        <ListItem dense button onClick={() => { closeMenu(); showHelpDialog(); }}>
          <ListItemIcon>
            <HelpIcon />
          </ListItemIcon>
          <ListItemText primary="Help" />
        </ListItem>
        <ListItem dense button onClick={() => { closeMenu(); showAboutDialog(); }}>
          <ListItemIcon>
            <AboutIcon />
          </ListItemIcon>
          <ListItemText primary="About" />
        </ListItem>
      </List>
      <ShareButton gameScore={gameScore} />
    </SwipeableDrawer>
  );
}
