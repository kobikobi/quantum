import React from 'react';
import classNames from 'classnames';
import { TileStates } from '../Constants';
import { Tilesets } from './Tilesets';

export default function TileCard({
  tilesetName, tileState, tilePicture, isFailReason, isCurrentTurnSelected, isMatched, onClickTile, debugMode,
}) {
  const pictureVisible = isCurrentTurnSelected || isFailReason;
  const tileCssClass = classNames(
    'Tile',
    'Card',
    pictureVisible ? 'PictureVisible' : 'PictureHidden',
    {
      PreviouslyRevealed: tileState === TileStates.revealed,
      Unknown: tileState === TileStates.unknown,
      FailReasonTile: isFailReason,
      Matched: isMatched,
    },
  );
  const glyph = tilePicture ? Tilesets[tilesetName].Glyphs[tilePicture.index] : undefined;
  if (tilePicture && !glyph) {
    throw new Error(`Glyph number ${tilePicture.index} is missing, add it to GlyphThemes.js.`);
  }
  return (
    <div className={tileCssClass} onClick={onClickTile}>
      <div className="FrontSide PaperTexture">
        {
            glyph
            && <div className={classNames('Glyph', glyph.cssClass)} dangerouslySetInnerHTML={{ __html: glyph.svg }} />
        }
      </div>
      <div className="BackSide PaperTexture">
        {
              debugMode && glyph
              && <div className={classNames('Debug', 'Glyph', glyph.cssClass)} dangerouslySetInnerHTML={{ __html: glyph.svg }} />
          }
      </div>
    </div>
  );
}
