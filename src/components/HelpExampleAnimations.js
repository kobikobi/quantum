export const intro = [
  [
    {},
  ],
  // first pair
  [
    {
      timeout: 500,
      tilePicture: 1,
      isCurrentTurnSelected: true,
    },
  ],
  [
    {
      tilePicture: 1,
      isCurrentTurnSelected: true,
    },
    {
      tilePicture: 2,
      isCurrentTurnSelected: true,
    },
  ],
  // flip back
  [
    {
      tilePicture: 1,
      isCurrentTurnSelected: false,
    },
    {
      tilePicture: 2,
      isCurrentTurnSelected: false,
    },
  ],
  // second pair
  [
    {
      timeout: 500,
      tilePicture: 1,
      isCurrentTurnSelected: false,
    },
    {
      tilePicture: 2,
      isCurrentTurnSelected: false,
    },
    {
      tilePicture: 1,
      isCurrentTurnSelected: true,
    },
  ],
  // match animation
  [
    {
      tilePicture: 1,
      isCurrentTurnSelected: true,
      foundMatchCurrentTurn: true,
    },
    {
      tilePicture: 2,
      isCurrentTurnSelected: false,
    },
    {
      tilePicture: 1,
      isCurrentTurnSelected: true,
      foundMatchCurrentTurn: true,
    },
  ],
  // flip back
  [
    {},
    {
      tilePicture: 2,
    },
  ],
  // third pair
  [
    {
      timeout: 500,
      tilePicture: 2,
      isCurrentTurnSelected: true,
    },
    {
      tilePicture: 2,
    },
  ],
  [
    {
      tilePicture: 2,
      isCurrentTurnSelected: true,
      foundMatchCurrentTurn: true,
    },
    {
      tilePicture: 2,
      isCurrentTurnSelected: true,
      foundMatchCurrentTurn: true,
    },
  ],
  [{ timeout: 300 }],
];

export const failSecondFlipNoMatch = [
  [
    {
    },
  ],
  // first pair
  [
    {
      timeout: 500,
      tilePicture: 3,
      isCurrentTurnSelected: true,
    },
  ],
  [
    {
      tilePicture: 3,
      isCurrentTurnSelected: true,
    },
    {
      tilePicture: 4,
      isCurrentTurnSelected: true,
    },
  ],
  // flip back
  [
    {
      tilePicture: 3,
      isCurrentTurnSelected: false,
    },
    {
      tilePicture: 4,
      isCurrentTurnSelected: false,
    },
  ],
  // second pair
  [
    {
      timeout: 500,
      tilePicture: 3,
      isCurrentTurnSelected: false,
    },
    {
      tilePicture: 4,
      isCurrentTurnSelected: false,
    },
    {
      tilePicture: 1,
      isCurrentTurnSelected: true,
    },
  ],
  // second pair - bad match - short delay before shoring red border
  [
    {
      timeout: 200,
      tilePicture: 3,
      isCurrentTurnSelected: true,
    },
    {
      tilePicture: 4,
      isCurrentTurnSelected: false,
    },
    {
      tilePicture: 1,
      isCurrentTurnSelected: true,
    },
  ],
  [
    {
      message: 'Game Over',
      tilePicture: 3,
      isCurrentTurnSelected: true,
      isFailReason: true,
    },
    {
      tilePicture: 4,
      isCurrentTurnSelected: false,
    },
    {
      tilePicture: 1,
      isCurrentTurnSelected: true,
    },
  ],
  [{ timeout: 300 }],
];

export const failShouldSelectMatch = [
  [
    {
    },
  ],
  // first pair
  [
    {
      timeout: 500,
      tilePicture: 3,
      isCurrentTurnSelected: true,
    },
  ],
  [
    {
      tilePicture: 3,
      isCurrentTurnSelected: true,
    },
    {
      tilePicture: 4,
      isCurrentTurnSelected: true,
    },
  ],
  // flip back
  [
    {
      tilePicture: 3,
      isCurrentTurnSelected: false,
    },
    {
      tilePicture: 4,
      isCurrentTurnSelected: false,
    },
  ],
  // second pair
  [
    {
      timeout: 500,
      tilePicture: 3,
      isCurrentTurnSelected: false,
    },
    {
      tilePicture: 4,
      isCurrentTurnSelected: false,
    },
    {
      tilePicture: 4,
      isCurrentTurnSelected: true,
    },
  ],
  // second pair - bad match
  [
    {
      timeout: 100,
      message: 'Game Over',
      tilePicture: 3,
      isCurrentTurnSelected: false,
    },
    {
      tilePicture: 4,
      isCurrentTurnSelected: false,
    },
    {
      tilePicture: 4,
      isCurrentTurnSelected: true,
    },
    {},
    {
      tilePicture: 1,
      isCurrentTurnSelected: true,
    },
  ],
  // second pair - bad match - flip failed reason
  [
    {
      message: 'Game Over',
      timeout: 1000,
      tilePicture: 3,
      isCurrentTurnSelected: false,
    },
    {
      tilePicture: 4,
      isCurrentTurnSelected: false,
      isFailReason: true,
    },
    {
      tilePicture: 4,
      isCurrentTurnSelected: true,
    },
    {},
    {
      tilePicture: 1,
      isCurrentTurnSelected: true,
    },
  ],
  [{ timeout: 300 }],
];
