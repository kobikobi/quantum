import React from 'react';

export default function HeaderStatisticsBox({ currentGameScorePairs, bestGameScorePairs }) {
  return (
    <div className="ToolbarScoreboard">
      <div className="Pair">
        <div className="Score Current">
          <label>Score</label>
          <data>{currentGameScorePairs}</data>
        </div>
        <div className="Score Best">
          <label>Best</label>
          <data>{bestGameScorePairs}</data>
        </div>
      </div>
    </div>
  );
}
