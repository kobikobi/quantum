import React from 'react';
import classNames from 'classnames';
import { TileTypes } from './Layouts';

export default function LayoutPreviewBase({
  renderTileCard, layout, forceNoRotation, tilesElementClass, tilesElementUniqueKey,
}) {
  function renderAnyTileType(layoutTile) {
    switch (layoutTile.type) {
      case TileTypes.Tile:
        return renderTileCard(layoutTile);
      case TileTypes.Newline:
        return <div className="LayoutNewLine" key={layoutTile.uniqueKey} />;
      case TileTypes.Space:
      case TileTypes.HalfSpace:
        return (
          <div
            className={
              classNames('Tile', 'Space', { HalfWidth: layoutTile.type === TileTypes.HalfSpace })
            }
            key={layoutTile.uniqueKey}
          />
        );
      default:
        return undefined;
    }
  }

  const wrapperCssClass = classNames(
    tilesElementClass,
    `LayoutSize${layout.sizeFactor}`,
    `LayoutWidth${layout.width}`,
    `LayoutHeight${layout.height}`,
    // disable rotation by always returning square layout
    forceNoRotation
      ? 'LayoutSquare'
      : {
        LayoutHorizontal: layout.width > layout.height,
        LayoutVertical: layout.width < layout.height,
        LayoutSquare: layout.width === layout.height,
      },
  );

  return (
    <div className={wrapperCssClass} key={tilesElementUniqueKey}>
      {layout.layout.map(renderAnyTileType)}
    </div>
  );
}
