import polyfill from '@babel/polyfill'; // todo: confirm this is working.
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App.jsx';

ReactDOM.render(<App />, document.getElementById('root'));
