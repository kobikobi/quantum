import React from 'react';
import promiseDelay from 'promise-delay';
import TileCard from './TileCard.jsx';
import { GameStates, TurnEndResults } from '../Constants';
import LayoutPreviewBase from './LayoutPreviewBase.jsx';

/**
 * Paint only the tiles and related events.
 */
export default function TileBoard({
  game, updateGameScore, onStateChanged, tilesetName, uiSettings, layout,
}) {
  const shortDelayMs = 900;
  function completeTurnAfterClick(turnEndResult) {
    if (!turnEndResult) {
      return;
    }
    if (turnEndResult.turnCount !== game.turnCount) {
      // if this turn already completed, ignore.
      // this can happen when the user clicks on a tile before the auto-hide timeout completes.
      return;
    }
    if (turnEndResult.result) {
      if (turnEndResult.result === TurnEndResults.Fail) {
        // on failure, don't end the turn, keep selected tiles visible.
        return;
      }
      game.completeTurn();
    }
    updateGameScore();
    onStateChanged();
  }

  function onClickTile(tile) {
    if (game.gameState === GameStates.showingOneTile
            && tile === game.firstShowingTile) {
      // user is clicking on the first showing tile again and again. Ignore the click.
      return;
    }

    if (game.gameState === GameStates.showingTwoTiles) {
      // possible if the user is clicking on a tile before the auto-hide delay completed.
      completeTurnAfterClick(game.lastTurnEndResult);
    }

    const pickTileResult = game.pickTile(tile);
    if (pickTileResult && pickTileResult.turnEndResult && pickTileResult.turnEndResult.result) {
      promiseDelay(shortDelayMs)
        .then(() => completeTurnAfterClick(pickTileResult.turnEndResult));
    }
    onStateChanged();
  }

  function renderTileCard(layoutTile) {
    const gameTile = game.board.tiles[layoutTile.tileIndex];
    if (!gameTile) return <span key={layoutTile.uniqueKey}>MIA</span>;
    const isFailReasonTile = game.lastTurnEndResult && game.lastTurnEndResult.failCauseTile;
    const isCurrentTurnSelected = gameTile === game.firstShowingTile
                                      || gameTile === game.secondShowingTile;
    const turnResult = game.lastTurnEndResult;
    const foundMatchCurrentTurn = isCurrentTurnSelected && turnResult
            && turnResult.result === TurnEndResults.FoundPair
            && turnResult.turnCount === game.turnCount;

    // we're using gameStartTimestamp as part of the key so that all tiles are re-generated when
    // a new game starts. Mainly this is done for the CSS animations.
    const tileUniqueKey = `${layoutTile.uniqueKey}_${game.gameStartTimestamp.getTime()}`;
    // todo: add classes, like row1, columns1, etc.
    return (
      <TileCard
        tileState={gameTile.state}
        tilePicture={gameTile.picture}
        key={tileUniqueKey}
        onClickTile={() => onClickTile(gameTile)}
        isCurrentTurnSelected={isCurrentTurnSelected}
        isFailReason={gameTile === isFailReasonTile}
        isMatched={foundMatchCurrentTurn}
        tilesetName={tilesetName}
        debugMode={uiSettings.debugMode}
      />
    );
  }

  return (
    <LayoutPreviewBase
      tilesElementUniqueKey={`gameTiles${game.gameStartTimestamp.getTime()}`}
      tilesElementClass={`Tiles Tileset${tilesetName}`}
      renderTileCard={renderTileCard}
      layout={layout}
    />
  );
}
