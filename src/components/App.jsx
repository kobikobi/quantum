import React, { useState } from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';
import MenuIcon from '@material-ui/icons/Menu';
import { withStyles, createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import { UiSettings, UiSettingsLocalStorageService } from './uiSettings';
import HeaderStatisticsBox from './HeaderStatisticsBox.jsx';
import GameArea from './GameArea.jsx';
import { LogoSvg } from './Tilesets';

const theme = createMuiTheme(
  {
    palette: {
      type: 'light',
      primary: {
        main: '#973c6d',
      },
      secondary: {
        main: '#2c877f', // #008c56
      },
      background: {
        default: '#ddd',
        paper: '#ddd', // the only paper I use the the about box.
        // default: '#777',
        // paper: '#777', //the only paper I use the the about box, it can be the same as default.
      },

    },
    // remove a warning, see https://material-ui.com/style/typography/#migration-to-typography-v2
    typography: {
      useNextVariants: true,
    },
    overrides: {
      MuiButton: {
        root: {
          // default line-height is 1.75, and the text isn't vertially aligned. 1.8 works better.
          lineHeight: 1.85,
        },
        label: {
          // align icon and label better.
          alignItems: 'center',
        },
      },
    },
  },
);

// #ba0060
// rgb(186, 0, 96)

const staticStyles = {
  root: {
    flexGrow: 1,
    // backgroundColor: 'rgba(186,0,96,0.3)',
    borderBottom: 'solid 1px rgba(0,0,0,0.4)',
    boxShadow: 'none',
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
};

const styles = currentTheme => ({
  toolbar: currentTheme.mixins.toolbar,
});

function App({ classes }) {
  const [uiSettings, setUiSettings] = useState(UiSettingsLocalStorageService.load() || new UiSettings());
  const [uiSettingsUpdated, setUiSettingsUpdated] = useState(0);
  const [showSettingsDrawer, setShowSettingsDrawer] = useState(false);
  const [score, setScore] = useState({ current: -2, best: -2 });

  function changeUiSetting(changeSettingsCallback) {
    if (changeSettingsCallback) changeSettingsCallback(uiSettings);
    setUiSettings(uiSettings);
    setUiSettingsUpdated(new Date());
    UiSettingsLocalStorageService.save(uiSettings);
  }

  function setGameScore(currentGameScore, bestScore) {
    // small bug here: I don't save this value in local storage, so I can't tell if a
    // loaded game is breaking the record. I can tell it matches it. this affects only the share button.
    const newPersonalRecord = bestScore === score.best + 1;
    const matchesPersonalRecord = currentGameScore === bestScore;
    setScore({
      current: currentGameScore, best: bestScore, newPersonalRecord, matchesPersonalRecord,
    });
  }

  return (
    <>
      <CssBaseline />
      <MuiThemeProvider theme={theme}>
        <div>
          <AppBar color="primary" position="fixed" style={staticStyles.root} className="tri Header">
            <Toolbar>
              <Typography variant="h6" color="inherit" style={staticStyles.grow} className="HeaderText">
                <div className="Glyth Dots Logo" dangerouslySetInnerHTML={{ __html: LogoSvg }} />
                Quantum
              </Typography>
              <HeaderStatisticsBox
                currentGameScorePairs={score.current}
                bestGameScorePairs={score.best}
              />
              <BottomNavigation
                color="primary"
                showLabels
                style={{ backgroundColor: 'rgba(0,0,0,0.1)' }}
              >
                <BottomNavigationAction
                  label="Options"
                  icon={<MenuIcon />}
                  color="primary"
                  style={{ color: 'white', minWidth: '70px' }}
                  onClick={() => setShowSettingsDrawer(true)}
                />
              </BottomNavigation>
            </Toolbar>
          </AppBar>
          <div className={classes.toolbar} />
          <i style={{ display: 'none' }}>Suzi is super cute!!! (sorry I removed this sweety, it was a placeholder)</i>
          <GameArea
            uiSettings={uiSettings}
            uiSettingsUpdated={uiSettingsUpdated}
            showSettingsDrawer={showSettingsDrawer}
            setSettingsDrawerVisible={setShowSettingsDrawer}
            changeUiSetting={changeUiSetting}
            gameScore={score}
            setGameScore={setGameScore}
          />
        </div>
      </MuiThemeProvider>
    </>
  );
}

export default withStyles(styles)(App);
