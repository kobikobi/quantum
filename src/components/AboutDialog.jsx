import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import CloseIcon from '@material-ui/icons/Close';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTwitter } from '@fortawesome/free-brands-svg-icons/faTwitter';
import { faGitlab } from '@fortawesome/free-brands-svg-icons/faGitlab';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons/faEnvelope';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
  // button: {
  //   margin: theme.theme.spacing(1),
  // },
  leftIcon: {
    marginRight: theme.spacing(1),
  },
  // rightIcon: {
  //   marginLeft: theme.spacing(1),
  // },
  // iconSmall: {
  //   fontSize: 20,
  // },
  dialogTitle: {
    paddingBottom: 0,
  },
  dialogContent: {
    paddingTop: 0,
  },
});

function AboutDialog({
  fullScreen, classes, hideAboutDialog, aboutDialogVisible,
}) {
  return (
    <Dialog
      fullScreen={fullScreen}
      open={aboutDialogVisible}
      onClose={hideAboutDialog}
      aria-labelledby="responsive-dialog-title"
    >
      <DialogTitle id="responsive-dialog-title" className={classes.dialogTitle}>About Quantum</DialogTitle>
      <DialogContent className={['DialogContent', classes.dialogContent]}>
        <Typography variant="body2" gutterBottom>
          Made by Kobi Lidershnider
          <br />
          <Button
            color="primary"
            autoFocus
            variant="contained"
            style={{ backgroundColor: '#1da1f2', textTransform: 'none' }}
            size="small"
            href="https://twitter.com/kobi"
            target="_blank"
          >
            <FontAwesomeIcon icon={faTwitter} />
            &nbsp;
            @kobi
          </Button>
          {' '}
          <Button
            color="primary"
            autoFocus
            variant="contained"
            style={{ backgroundColor: '#555', textTransform: 'none' }}
            size="small"
            href="mailto:kobikobi@gmail.com"
            target="_blank"
          >
            <FontAwesomeIcon icon={faEnvelope} />
            &nbsp;
            Email
          </Button>
          {' '}
          <Button
            color="primary"
            autoFocus
            variant="contained"
            style={{ backgroundColor: '#fc6d26', textTransform: 'none' }}
            size="small"
            href="https://gitlab.com/kobikobi/quantum"
            target="_blank"
          >
            <FontAwesomeIcon icon={faGitlab} />
            &nbsp;
            View Source
          </Button>
        </Typography>

        <Typography variant="h6">Icons</Typography>
        <Typography variant="body2" gutterBottom>
          <a href="https://www.flaticon.com/packs/abstract-elements" target="_blank" rel="noreferrer">Abstract Elements</a>
          ,
          {' '}
          <a href="https://www.flaticon.com/packs/abstract-glyph" target="_blank" rel="noreferrer">Abstract Glyph</a>
          {' '}
          designed by Alfredo Hernandez from Flaticon.
          <br />
        </Typography>
        <Typography variant="body2" gutterBottom>
          <a href="https://www.flaticon.com/packs/japanese-flags" target="_blank" rel="noreferrer">Japanese Flags</a>
          ,
          {' '}
          <a href="https://www.flaticon.com/packs/sea-life-8" target="_blank" rel="noreferrer">Sea Life</a>
          {' '}
          designed by Freepik from Flaticon.
          <br />

          License:
          {' '}
          <a href="https://file000.flaticon.com/downloads/license/license.pdf" target="_blank" rel="noreferrer">
            {' '}
            Flaticon Free License (with attribution)
          </a>
          <br />
        </Typography>
        <Typography variant="h6">Graphics</Typography>
        <Typography variant="body2" gutterBottom>
          Backgrounds by
          {' '}
          <a href="https://twitter.com/BumpSetCreative" target="_blank" rel="noreferrer">Matthew Lipman</a>
          {' '}
          at
          {' '}
          <a href="https://www.svgbackgrounds.com" target="_blank" rel="noreferrer">SVGBackgrounds.com</a>
          <br />
          License:
          {' '}
          <a href="https://www.svgbackgrounds.com/license/" target="_blank" rel="noreferrer">CC BY 4.0</a>
        </Typography>
        <Typography variant="h6">Thanks</Typography>
        <Typography variant="body2" gutterBottom>
          <a href="https://realfavicongenerator.net/" target="_blank" rel="noreferrer">Favicon Generator. For real.</a>
          <br />
          <a href="http://www.colorzilla.com/gradient-editor/" target="_blank" rel="noreferrer">Ultimate CSS Gradient Generator</a>
          <br />
          <a href="https://maskable.app/">https://maskable.app/</a>
        </Typography>
        <Typography variant="h6">Privacy Policy</Typography>
        <Typography variant="body2">
          Quantum is hosted on GitLab pages, see
          {' '}
          <a href="https://about.gitlab.com/privacy/" target="_blank" rel="noreferrer">GitLab Privacy Policy</a>
        </Typography>
        <Typography variant="body2">
          We use Google Analytics to track the number of visitors and new games.
          Personal information is not collected. Data such as location,
          browser, IP, and operating system is collected. Google Analytics
          uses cookies to track user sessions and visits, see
          {' '}
          <a
            href="https://developers.google.com/analytics/devguides/collection/analyticsjs/cookie-usage"
            target="_blank"
            rel="noreferrer"
          >
            Google Analytics Cookie Usage on Websites
          </a>
          ,
          {' '}
          <a href="https://support.google.com/analytics/answer/6004245" target="_blank" rel="noreferrer">
            Information for Visitors of Sites and Apps Using Google Analytics
          </a>
          .
        </Typography>
        <Typography variant="body2">
          Game progress and best score are saved on your browser's
          {' '}
          {' '}
          <a href="https://en.wikipedia.org/wiki/Web_storage#Local_and_session_storage" target="_blank" rel="noreferrer">
            local storage
          </a>
          {' '}
          - they are never sent and never leave your computer.
        </Typography>
        <Typography variant="body2">
          We respect your browser's
          {' '}
          <a href="https://www.eff.org/issues/do-not-track" target="_blank" rel="noreferrer">Do Not Track</a>
          {' '}
          policy
          and do not load Google Analytics if it is set.
          I also encourage everyone not to rely on voluntary compliance, and install
          an ad blocker and privacy tool like
          {' '}
          <a href="https://github.com/gorhill/uBlock" target="_blank" rel="noreferrer">uBlock Origin</a>
          .
          {' '}
        </Typography>
      </DialogContent>
      <DialogActions>
        <Button onClick={hideAboutDialog} color="primary" autoFocus variant="contained">
          <CloseIcon className={classes.leftIcon} />
          Close
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default withStyles(styles)(withMobileDialog()(AboutDialog));
