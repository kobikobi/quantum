import React from 'react';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Drawer from '@material-ui/core/Drawer';
import NewGameIcon from '@material-ui/icons/ViewModule';
import { withStyles } from '@material-ui/core/styles';
import { GameStates } from '../Constants';
import ShareButton from './ShareButton.jsx';
import { useRandomItem } from '../services/useRandom';

const styles = theme => ({
  leftIcon: {
    marginRight: theme.spacing(1),
  },
});

const gameOverTitles = ['Oops', 'Wrong one…', 'You forgot', 'Game Over'];

function GameOverDrawer({
  game, classes, onClickNewGame, gameScore,
}) {
  const { lastTurnEndResult } = game;
  const gameOverTitle = useRandomItem(gameOverTitles, [game]);

  return (
    <Drawer
      anchor="bottom"
      PaperProps={{ padding: '10px' }}
      open={game.gameState === GameStates.failed}
      onClose={onClickNewGame}
    >
      <div className="GameOver">
        <Typography variant="h6">
          {gameOverTitle}
        </Typography>
        <Typography variant="subtitle1" gutterBottom>
          {lastTurnEndResult && lastTurnEndResult.failReason}
        </Typography>
        <Button variant="contained" color="primary" onClick={onClickNewGame}>
          <NewGameIcon className={classes.leftIcon} />
          Try Again
        </Button>
        {' '}
        <ShareButton gameScore={gameScore} />
      </div>
    </Drawer>
  );
}

export default withStyles(styles)(GameOverDrawer);
