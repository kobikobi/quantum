import React from 'react';
import LayoutPreviewBase from './LayoutPreviewBase.jsx';

export default function LayoutPreview({ layout }) {
  const tilesElementUniqueKey = `LayoutPreview${layout.name}`;

  function renderTileCard(layoutTile) {
    return <div className="Tile Card" key={layoutTile.uniqueKey} />;
  }

  return (
    <LayoutPreviewBase
      tilesElementUniqueKey={tilesElementUniqueKey}
      tilesElementClass="TilesPreview"
      renderTileCard={renderTileCard}
      layout={layout}
    />
  );
}
