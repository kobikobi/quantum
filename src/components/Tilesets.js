import T1 from '../images/tiles/fukuoka-japan-flag-symbol.svg';
import T2 from '../images/tiles/dots.svg';
import T3 from '../images/tiles/osaka-japanese-flag-symbol.svg';
import T4 from '../images/tiles/tokyo-japanese-flag-symbol-like-a-sun.svg';
import T5 from '../images/tiles/miyagi-japan-flag-abstract-symbol.svg';
import T6 from '../images/tiles/hexagon.svg';
import T7 from '../images/tiles/triangle.svg';
import T8 from '../images/tiles/triangles.svg';
import T9 from '../images/tiles/rhombus-1.svg';
import T10 from '../images/tiles/hexagons.svg';
import T11 from '../images/tiles/fukui-japanese-symbol.svg';

import C1 from '../images/tiles/sea/002-crab.svg';
import C2 from '../images/tiles/sea/050-tortoise.svg';
import C3 from '../images/tiles/sea/004-starfish.svg';
import C4 from '../images/tiles/sea/044-squid.svg';
import C5 from '../images/tiles/sea/048-snail.svg';
import C6 from '../images/tiles/sea/049-seahorse.svg';
import C7 from '../images/tiles/sea/045-whale.svg';
import C8 from '../images/tiles/sea/042-fish-2.svg';
import C9 from '../images/tiles/sea/034-shark.svg';
import C10 from '../images/tiles/sea/039-jellyfish.svg';
import C11 from '../images/tiles/sea/003-fish-10.svg';

const Tilesets = Object.freeze({
  Abstract: {
    CssClass: 'Abstract',
    Glyphs: [
      { svg: T2, cssClass: 'Dots' },
      { svg: T1, cssClass: 'Fukuoka' },
      { svg: T3, cssClass: 'Osaka' },
      { svg: T4, cssClass: 'Tokyo' },
      { svg: T5, cssClass: 'Miyagi' },
      { svg: T6, cssClass: 'Hexagon' },
      { svg: T7, cssClass: 'Triangle' },
      { svg: T8, cssClass: 'FourTriangles' },
      { svg: T9, cssClass: 'RhombusCircle' },
      { svg: T10, cssClass: 'ThreeHexagons' },
      { svg: T11, cssClass: 'Fukui' },
    ],
  },
  Sea: {
    CssClass: 'SeaLife',
    Glyphs: [
      { svg: C4, cssClass: 'Squid' },
      { svg: C1, cssClass: 'Crab' },
      { svg: C2, cssClass: 'Tortoise' },
      { svg: C3, cssClass: 'Starfish' },
      { svg: C5, cssClass: 'Snail' },
      { svg: C6, cssClass: 'Seahorse' },
      { svg: C7, cssClass: 'Whale' },
      { svg: C8, cssClass: 'Blowfish' },
      { svg: C9, cssClass: 'Shark' },
      { svg: C10, cssClass: 'Jellyfish' },
      { svg: C11, cssClass: 'Pinkfish' },
    ],
  },
});

const TilesetNames = Object.freeze(Object.keys(Tilesets));
const LogoSvg = T2;

export {
  Tilesets, TilesetNames, LogoSvg,
};
