import React, { useState, useEffect, useMemo } from 'react';
import classNames from 'classnames';
import TileBoard from './TileBoard.jsx';
import Game from '../QuantumGame';
import { GameStates, GameModes } from '../Constants';
import SettingsMenu from './SettingsMenu.jsx';
import AboutDialog from './AboutDialog.jsx';
import HelpDialog from './HelpDialog.jsx';
import { Layouts } from './Layouts';
import Scoreboard from '../services/Scoreboard';
import GameStateSaver from '../services/GameStateSaver';
import GameRandomPicturePicker from '../services/GameRandomPicturePicker';
import GameOverDrawer from './GameOverDrawer.jsx';
import Monitoring from '../services/MetricsTracking';

const scoreboard = new Scoreboard();
const gameSaver = new GameStateSaver();

function getGameMode(uiSettings) {
  return uiSettings.casualMode ? GameModes.Casual : GameModes.Brutal;
}

function calculateUiSettingsWillChangeAtNextGame(currentGame, currentLaout, currentTilesetName, uiSettings) {
  return (currentGame.gameMode !== getGameMode(uiSettings))
    || (currentLaout.name !== uiSettings.layoutName)
    || (currentTilesetName !== uiSettings.tileset);
}

function loadSavedGame() {
  const data = gameSaver.load();
  if (!data) return undefined;
  const { game, layoutName, tilesetName } = data;
  if (!game) return undefined;
  // if we saved a failed game, no point in loading it like some turnip.
  if (game.gameState === GameStates.failed) return undefined;
  // if this is the begining of a game, don't load it. Avoid the "new game" message.
  if (game.turnCount === 0 && game.gameState === GameStates.turnStart) return undefined;
  window.__quantumGame = game;
  // mark the game as loaded at this turn, to display a different animation.
  game.loadedStateSnapshot = { turnCount: game.turnCount, gameState: game.gameState };
  const layout = Layouts.get(layoutName);
  return { game, layout, tilesetName };
}

function saveGameToLocalStorage(game, layoutName, tilesetName) {
  gameSaver.save(game, layoutName, tilesetName);
}

function createNewGame(uiSettings) {
  const gameMode = getGameMode(uiSettings);
  const layout = Layouts.get(uiSettings.layoutName);
  const tilesetName = uiSettings.tileset;
  if (!layout) throw new Error(`cannot find layout \`${uiSettings.layoutName}\``);
  const pictures = GameRandomPicturePicker(tilesetName, layout.pictureCount);
  const game = new Game(undefined, gameMode, layout.tileCount, pictures);
  window.__quantumGame = game;
  return { game, layout, tilesetName };
}

function getGameUniqueKey(game) {
  return `Game_${game.gameStartTimestamp.getTime()}`;
}

export default function GameArea({
  uiSettings, uiSettingsUpdated, setGameScore, changeUiSetting, showSettingsDrawer, setSettingsDrawerVisible, gameScore,
}) {
  const {
    game: loadedGame,
    layout: loadedLayout,
    tilesetName: loadedTilesetName,
  } = useMemo(() => loadSavedGame() || createNewGame(uiSettings), []);
  const [game, setGame] = useState(loadedGame);
  const [gameUpdted, setGameUpdated] = useState(0);
  const [layout, setLayout] = useState(loadedLayout);
  const [tilesetName, setTilesetName] = useState(loadedTilesetName);
  const [aboutDialogVisible, setAboutDialogVisible] = useState(false);
  const [helpDialogVisible, setHelpDialogVisible] = useState(false);
  const [showFirstVisitHelpDialog, setShowFirstVisitHelpDialog] = useState(scoreboard.createdNewScoreboardForNewUser);

  // small delay before showing the help window, gives the loading animation time to complete
  if (showFirstVisitHelpDialog) {
    setShowFirstVisitHelpDialog(false);
    setTimeout(() => { setHelpDialogVisible(true); }, 300);
  }

  function updateGameScore() {
    const currentScore = game.pairsFoundCount;
    const best = scoreboard.setCurrentScore(currentScore, layout.name, game.gameMode);
    setGameScore(currentScore, best);
  }

  // reset the game score once after loading a saved game, or starting a new game
  useEffect(() => updateGameScore(), [game]);

  function onClickNewGame() {
    const { game: newGame, layout: newLayout, tilesetName: newTilesetName } = createNewGame(uiSettings);
    setGame(newGame);
    setLayout(newLayout);
    setTilesetName(newTilesetName);
    saveGameToLocalStorage(newGame, newLayout.name, newTilesetName);
    updateGameScore();
    Monitoring.eventTrackingNewGameStarted();
  }

  const uiSettingsWillChangeAtNextGame = calculateUiSettingsWillChangeAtNextGame(game, layout, tilesetName, uiSettings);
  // start new game automatically if settings change at turn 0
  if (uiSettingsWillChangeAtNextGame && game.turnCount === 0) {
    onClickNewGame();
  }

  /**
     * Trigger a state changed. Useful when a child componenet made changes to this.state.game.
     */
  function onStateChanged() {
    setGameUpdated(new Date());
    saveGameToLocalStorage(game, layout.name, tilesetName);
  }

  const isNewGameAndNoTiles = game.turnCount === 0 && game.gameState === GameStates.turnStart;
  // isOldGameLoadedNow - first turn after opening the page and loading an old saved game.
  const isOldGameLoadedNow = game.loadedStateSnapshot
    && game.loadedStateSnapshot.turnCount === game.turnCount
    && game.loadedStateSnapshot.gameState === game.gameState;
  const gameUniqueKey = getGameUniqueKey(game);
  return (
    <div className={classNames(
      'GameArea',
      {
        HelperMarkKnownTiles: uiSettings.markUnknownTiles,
        NewGameNoTiles: isNewGameAndNoTiles,
        OldGameLoadedNow: isOldGameLoadedNow,
      },
    )}
    >
      <TileBoard
        game={game}
        gameUpdated={gameUpdted}
        uiSettings={uiSettings}
        key={gameUniqueKey}
        onStateChanged={onStateChanged}
        layout={layout}
        tilesetName={tilesetName}
        updateGameScore={updateGameScore}
      />
      <GameOverDrawer game={game} onClickNewGame={onClickNewGame} gameScore={gameScore} />
      <SettingsMenu
        showSettingsDrawer={showSettingsDrawer}
        setSettingsDrawerVisible={setSettingsDrawerVisible}
        onClickNewGame={onClickNewGame}
        gameIsCasualMode={game.gameMode === GameModes.Casual}
        changeUiSetting={changeUiSetting}
        showHelpDialog={() => setHelpDialogVisible(true)}
        showAboutDialog={() => setAboutDialogVisible(true)}
        uiSettingsWillChangeAtNextGame={uiSettingsWillChangeAtNextGame}
        uiSettings={uiSettings}
        uiSettingsUpdated={uiSettingsUpdated}
        gameScore={gameScore}
      />
      <AboutDialog
        aboutDialogVisible={aboutDialogVisible}
        hideAboutDialog={() => { setAboutDialogVisible(false); }}
      />
      <HelpDialog
        helpDialogVisible={helpDialogVisible}
        hideHelpDialog={() => { setHelpDialogVisible(false); }}
        uiSettings={uiSettings}
        changeUiSetting={changeUiSetting}
        tilesetName={tilesetName}
        // slight change to the button text on new game, especially for auto-opened help dialog on first visit
        isNewGameAndNoTiles={isNewGameAndNoTiles}
      />
    </div>
  );
}
