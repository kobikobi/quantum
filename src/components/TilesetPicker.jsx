import React from 'react';
import Button from '@material-ui/core/Button';
import classNames from 'classnames';
import { Tilesets, TilesetNames } from './Tilesets';

export default function TilesetPicker({ currentTilesetName, changeUiSetting }) {
  const buttons = TilesetNames.map(tilesetName => {
    const tileset = Tilesets[tilesetName].Glyphs[0];
    const isSelected = currentTilesetName === tilesetName;
    const buttonStyle = { borderRadius: 0 };
    return (
      <Button
        width={64} // variant="contained" color="primary"
        key={`TilesetPickerButton${tilesetName}`}
        style={buttonStyle}
        className={classNames('BigButtonCheckbox', 'TilsetPickerButton', { Selected: isSelected })}
        onClick={() => changeUiSetting(ui => { ui.tileset = tilesetName; })}
      >
        <div className={classNames('Glyph', tileset.cssClass)} dangerouslySetInnerHTML={{ __html: tileset.svg }} />
      </Button>
    );
  });

  return (
    <div className="TilesetPicker">
      {buttons}
    </div>
  );
}
