/**
 * Return all pictures that are not currently used, with duplicates.
 * A picture may be returned 0 times, 1 time, or 2 times.
 */
export function* getPotentialPictures(pictures, usedPictures) {
  const m = new Map(pictures.map(p => [p, 2]));
  usedPictures.forEach(p => {
    m.set(p, m.get(p) - 1);
  });
  for (const [p, times] of m) {
    // return p 0,1, or 2 times. Could have been a for loop.
    if (times >= 1) yield p;
    if (times >= 2) yield p;
  }
}

const gameRandomPolicy = {
  selectPicture: (pictures, board) => {
    // todo: use better random.
    const usedPictures = board.getKnownTiles().map(t => t.picture);
    if (usedPictures.length >= pictures.length * 2) {
      throw new Error('There are no more pictures to choose from, you need more tiles.');
    }
    // it would be better to keep the set as part of the game state, instead of calculating it on each turn.
    const m = Array.from(getPotentialPictures(pictures, usedPictures));
    // this is the same assert as the above - checking anyway for sanity.
    if (m.size === 0) {
      throw new Error('There are no more pictures to choose from, you need more tiles.');
    }
    // take all unused pictures, and all pictures used only once.
    const randomIndex = Math.floor(Math.random() * m.length);
    return m[randomIndex];
  },
};

export class ArrayBasedMock {
  constructor(pictureOrderArray) {
    this.pictureOrderArray = pictureOrderArray;
  }

  selectPicture(pictures) {
    if (this.pictureOrderArray.length === 0) {
      throw new Error('The array used in the constructor must not be empty. You can always push() element to the array.');
    }
    const index = this.pictureOrderArray.shift(); // return and remove first element from the array.
    return pictures[index];
  }
}

export const DefaultPolicy = gameRandomPolicy;
export const reasonableRandom = gameRandomPolicy;
