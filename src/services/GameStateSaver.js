import { serializeGameToSimpleObject, deserializeSimpleObjectToGame } from './GameSerializer';
import { ensureLayoutNameExists, ensureTilesetExists } from '../components/uiSettings';

const defaultSettingsName = 'game';

export default class GameStateSaver {
  constructor(settingsName) {
    this.settingsName = settingsName || defaultSettingsName;
  }

  save(game, layoutName, tilesetName) {
    // try to do it non-blocking. This is probbaly a drop in the ocean next to the whole of React...
    setTimeout(() => {
      try {
        const gameState = serializeGameToSimpleObject(game);
        const data = { game: gameState, layoutName, tilesetName };
        localStorage.setItem(this.settingsName, JSON.stringify(data));
      } catch (err) {
        console.error(err);
      }
    }, 50);
  }

  load() {
    try {
      const serializedSavedGame = localStorage.getItem(this.settingsName);
      if (!serializedSavedGame) return undefined;
      const data = JSON.parse(serializedSavedGame);
      data.game = deserializeSimpleObjectToGame(data.game);
      data.layoutName = ensureLayoutNameExists(data.layoutName);
      data.tilesetName = ensureTilesetExists(data.tilesetName);
      return data;
    } catch (err) {
      console.error(err);
      return undefined;
    }
  }
}
