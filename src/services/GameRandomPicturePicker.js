import { Tilesets } from '../components/Tilesets';
import Picture from '../Picture';

// based on https://stackoverflow.com/a/49479872/7586 , thanks Rodrigo!
function randomElements(array, n) {
  // sort by random value.
  return array
    .map(x => ({ x, r: Math.random() }))
    .sort((a, b) => a.r - b.r)
    .map(a => a.x)
    .slice(0, n);
}

/** Pick `pictureCount` pictures from a tileset by tileset name.  */
export default function pickRandomPictures(tilesetName, pictureCount) {
  const tilesetGlyphes = Tilesets[tilesetName].Glyphs;
  if (tilesetGlyphes.length < pictureCount) {
    throw new Error(`Not enough glyphs. Layout ${tilesetName} has ${tilesetGlyphes.length} glyphs, `
              + `but the layout requires ${pictureCount}`);
  }
  const pictures = tilesetGlyphes.map((g, i) => new Picture(g.cssClass, i));
  return randomElements(pictures, pictureCount);
}
