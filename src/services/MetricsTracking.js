function doNotTrack() {
  return navigator.doNotTrack === '1';
}

function sendEvent(event, action, label) {
  // ga('send','event',...) isn't working at all.
  // workaround: https://stackoverflow.com/a/22971643/7586
  // eslint-disable-next-line no-undef
  const tracker = ga.getAll()[0];
  tracker.send('event', event, action, label);
}

function eventTrackingNewGameStarted() {
  try {
    if (doNotTrack()) return;
    if (!window.ga) return;
    sendEvent('Game', 'ClickNewGame');
  } catch {
    // eslint-disable-next-line no-empty
  }
}

export default { eventTrackingNewGameStarted };
