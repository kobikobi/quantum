const defaultSettingsName = 'score';

function saveSettings(score, settingsName) {
  try {
    localStorage.setItem(settingsName, JSON.stringify(score));
  } catch (err) {
    console.error(err);
  }
}
function loadSettings(settingsName) {
  try {
    const serializedSavedScore = localStorage.getItem(settingsName);
    if (!serializedSavedScore) return undefined;
    const savedScores = JSON.parse(serializedSavedScore);
    return savedScores;
  } catch (err) {
    console.error(err);
    return undefined;
  }
}

export default class Scoreboard {
  constructor(settingsName) {
    this.settingsName = settingsName || defaultSettingsName;
    this.data = loadSettings(this.settingsName);
    if (this.data === undefined) {
      // this works as long as Scoreboard is used only once. If we need it someplace else, change to a singleton.
      this.createdNewScoreboardForNewUser = true;
      this.data = {};
    }
  }

  /**
     * Return the highest value ever, even if it wasn't changed.
     */
  setMetric(value, layoutName, gameMode, metric) {
    // this code is not clever and that's OK.
    this.data[layoutName] = this.data[layoutName] || {};
    this.data[layoutName][gameMode] = this.data[layoutName][gameMode] || {};
    this.data[layoutName][gameMode][metric] = Math.max(value, this.data[layoutName][gameMode][metric] || 0);
    saveSettings(this.data, this.settingsName);
    return this.data[layoutName][gameMode][metric];
  }

  /**
     * Set the score of the current game. Returns the best score for the same game setting.
     */
  setCurrentScore(newScore, layoutName, gameMode) {
    this.currentScore = newScore; // not sure this belongs here. the game has this data anyway.
    return this.setMetric(newScore, layoutName, gameMode, 'best');
  }

  getBestScore(layoutName, gameMode) {
    // set the score to 0 in case it is undefined. This also returns the best score.
    return this.setCurrentScore(0, layoutName, gameMode);
  }
}
