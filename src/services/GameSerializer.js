import Game from '../QuantumGame';
import Picture from '../Picture';

// this may have been simple if Game had been immutable.

/**
     * Take a game and return a simple object represeting its state.
     */
export function serializeGameToSimpleObject(game) {
  function serializeTile(tile) {
    if (!tile) return tile;
    return {
      state: tile.state,
      pictureIndex: tile.picture && tile.picture.index,
      // tileIndex:tileIndex,
    };
  }

  function getTileIndex(tile) {
    if (!tile) return tile;
    const tileIndex = game.board.tiles.findIndex(t => t === tile);
    if (tileIndex === -1) throw new Error('canot find this tile in the game board. it should have been there.');
    return tileIndex;
  }

  return {
    pictures: game.pictures.map(p => ({ name: p.name, globalIndex: p.index })),
    tileCount: game.board.tiles.length,
    board: game.board.tiles.map(serializeTile),
    gameState: game.gameState,
    firstShowingTile: getTileIndex(game.firstShowingTile),
    secondShowingTile: getTileIndex(game.secondShowingTile),
    turnCount: game.turnCount,
    pairsFoundCount: game.pairsFoundCount,
    gameMode: game.gameMode,
    gameStartTimestamp: game.gameStartTimestamp,
    firstShowingTilePreviouslyKnown: game.firstShowingTilePreviouslyKnown,
    secondShowingTilePreviouslyKnown: game.secondShowingTilePreviouslyKnown,
    lastTurnEndResult: game.lastTurnEndResult
            && {
              result: game.lastTurnEndResult.result,
              turnCount: game.lastTurnEndResult.turnCount,
              firstShowingTile: getTileIndex(game.lastTurnEndResult.firstShowingTile),
              secondShowingTile: getTileIndex(game.lastTurnEndResult.secondShowingTile),
              failCauseTile: getTileIndex(game.lastTurnEndResult.failCauseTile),
              failReason: game.lastTurnEndResult.failReason,
            },
  };
}

export function deserializeSimpleObjectToGame(simpleObject) {
  if (!simpleObject) return null;

  const gamePictures = simpleObject.pictures.map(p => new Picture(p.name, p.globalIndex));

  const game = new Game(undefined, simpleObject.gameMode, simpleObject.board.length, gamePictures);

  /** Find the Picture instance already created for the game. */
  function getGamePicture(globalPictureIndex) {
    if (globalPictureIndex === undefined) return undefined;
    return game.pictures.find(p => p.index === globalPictureIndex);
  }

  game.board.tiles.forEach((tile, index) => {
    const simpleTile = simpleObject.board[index];
    tile.state = simpleTile.state;
    tile.picture = getGamePicture(simpleTile.pictureIndex);
  });

  function getTileByIndex(tileIndex) {
    if (tileIndex === undefined) return undefined;
    return game.board.tiles[tileIndex];
  }

  game.gameStartTimestamp = new Date(simpleObject.gameStartTimestamp);
  game.gameState = simpleObject.gameState;
  game.turnCount = simpleObject.turnCount;
  game.pairsFoundCount = simpleObject.pairsFoundCount;
  game.firstShowingTile = getTileByIndex(simpleObject.firstShowingTile);
  game.firstShowingTilePreviouslyKnown = simpleObject.firstShowingTilePreviouslyKnown;
  game.secondShowingTile = getTileByIndex(simpleObject.secondShowingTile);
  game.secondShowingTilePreviouslyKnown = simpleObject.secondShowingTilePreviouslyKnown;

  game.lastTurnEndResult = simpleObject.lastTurnEndResult
        && {
          result: simpleObject.lastTurnEndResult.result,
          failReason: simpleObject.lastTurnEndResult.failReason,
          turnCount: simpleObject.lastTurnEndResult.turnCount,
          firstShowingTile: getTileByIndex(simpleObject.lastTurnEndResult.firstShowingTile),
          secondShowingTile: getTileByIndex(simpleObject.lastTurnEndResult.secondShowingTile),
          failCauseTile: getTileByIndex(simpleObject.lastTurnEndResult.failCauseTile),
        };

  return game;
}

/*
Example of game data as JSON

"{
  "pictures": [
    {
      "name": "apple",
      "index": 0
    },
    {
      "name": "banana",
      "index": 1
    },
    {
      "name": "dog",
      "index": 2
    },
    {
      "name": "bird",
      "index": 3
    },
    {
      "name": "dinosaur",
      "index": 4
    },
    {
      "name": "whale",
      "index": 5
    },
    {
      "name": "monkey",
      "index": 6
    },
    {
      "name": "rhinoceros",
      "index": 7
    }
  ],
  "randomPolicy": {},
  "board": {
    "tiles": [
      {
        "picture": {
          "name": "dinosaur",
          "index": 4
        },
        "state": "Revealed"
      },
      {
        "picture": {
          "name": "apple",
          "index": 0
        },
        "state": "Revealed"
      },
      {
        "picture": {
          "name": "banana",
          "index": 1
        },
        "state": "Revealed"
      },
      {
        "state": "Unknown"
      },
      {
        "state": "Unknown"
      },
      {
        "picture": {
          "name": "rhinoceros",
          "index": 7
        },
        "state": "Revealed"
      },
      {
        "picture": {
          "name": "monkey",
          "index": 6
        },
        "state": "Revealed"
      },
      {
        "state": "Unknown"
      },
      {
        "state": "Unknown"
      },
      {
        "state": "Unknown"
      },
      {
        "state": "Unknown"
      },
      {
        "state": "Unknown"
      },
      {
        "state": "Unknown"
      },
      {
        "state": "Unknown"
      },
      {
        "state": "Unknown"
      }
    ]
  },
  "gameState": "One card showing",
  "firstShowingTile": {
    "picture": {
      "name": "banana",
      "index": 1
    },
    "state": "Revealed"
  },
  "turnCount": 2,
  "pairsFoundCount": 0,
  "gameMode": "Brutal",
  "gameStartTimestamp": "2018-11-16T14:16:45.449Z",
  "firstShowingTilePreviouslyKnown": false,
  "lastTurnEndResult": {
    "result": "Revealed two unknown tiles",
    "firstShowingTile": {
      "picture": {
        "name": "rhinoceros",
        "index": 7
      },
      "state": "Revealed"
    },
    "secondShowingTile": {
      "picture": {
        "name": "monkey",
        "index": 6
      },
      "state": "Revealed"
    },
    "turnCount": 1
  }
}"

*/
