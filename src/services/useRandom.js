import { useMemo } from 'react';

/**
 * return a random item from the array. the same item will be reuturned as long as `dependencies` doesn't chanes
 * @param {[]} array a collection of possible values
 */
export function useRandomItem(array, dependencies) {
  return useMemo(() => {
    const index = Math.floor(Math.random() * array.length);
    return array[index];
  }, dependencies);
}
