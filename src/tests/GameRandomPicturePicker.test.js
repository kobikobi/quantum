import pickRandomPictures from '../services/GameRandomPicturePicker';
import { TilesetNames } from '../components/Tilesets';

const tileset = TilesetNames[0];

function testCount(pictureCount) {
  const set = pickRandomPictures(tileset, pictureCount);
  expect(set).toHaveLength(pictureCount);
}

test('1 picture', () => testCount(1));
test('2 pictures', () => testCount(2));
test('5 pictures', () => testCount(5));
test('9 pictures', () => testCount(9));
test('66 pictures', () => {
  expect(() => pickRandomPictures(tileset, 66)).toThrow();
});
