import Tile from '../Tile';
import Picture from '../Picture';
import Board from '../Board';
import { reasonableRandom, getPotentialPictures } from '../GameRandomPolicy';

const tileCount = 10;

function createBoard() {
  const pictures = ['a', 'b', 'c'].map(s => new Picture(s));
  const tiles = new Array(tileCount).fill().map(() => new Tile());
  return [new Board(tiles), pictures];
}

test('never produce third tile with same picture - only one option', () => {
  const [board, pictures] = createBoard();
  board.tiles[0].reveal(pictures[0]);
  board.tiles[1].reveal(pictures[0]);
  board.tiles[2].reveal(pictures[1]);
  board.tiles[3].reveal(pictures[1]);
  // try to get a random picture based on the board 10 times.
  // they should all return the unused picture.
  for (let i = 0; i < 10; i++) {
    const nextPicture = reasonableRandom.selectPicture(pictures, board);
    expect(nextPicture).toBe(pictures[2]);
  }
});

test('never produce third tile with same picture - only one option - one revealed', () => {
  const [board, pictures] = createBoard();
  board.tiles[0].reveal(pictures[0]);
  board.tiles[1].reveal(pictures[0]);
  board.tiles[2].reveal(pictures[1]);
  board.tiles[3].reveal(pictures[1]);
  board.tiles[4].reveal(pictures[2]);
  // try to get a random picture based on the board 10 times.
  // they should all return the unused picture.
  for (let i = 0; i < 10; i++) {
    const nextPicture = reasonableRandom.selectPicture(pictures, board);
    expect(nextPicture).toBe(pictures[2]);
  }
});

test('never produce third tile with same picture - only one option - used up all options, throw.', () => {
  const [board, pictures] = createBoard();
  board.tiles[0].reveal(pictures[0]);
  board.tiles[1].reveal(pictures[0]);
  board.tiles[2].reveal(pictures[1]);
  board.tiles[3].reveal(pictures[1]);
  board.tiles[4].reveal(pictures[2]);
  board.tiles[5].reveal(pictures[2]);
  expect(() => reasonableRandom.selectPicture(pictures, board)).toThrow();
});

test('getPotentialPictures simple test', () => {
  const result = Array.from(getPotentialPictures([1, 2, 3, 4, 5], [1, 1, 2, 2, 3, 3, 4]));
  expect(result).toEqual([4, 5, 5]);
});

test('getPotentialPictures should be empty', () => {
  const result = Array.from(getPotentialPictures([1, 2], [1, 1, 2, 2]));
  expect(result).toHaveLength(0);
});

test('getPotentialPictures should double the array', () => {
  const result = Array.from(getPotentialPictures([1, 2, 3], []));
  expect(result).toEqual([1, 1, 2, 2, 3, 3]);
});
