import { TileTypes, _layoutParserHelper } from '../components/Layouts';

test('single line', () => {
  const s = 'xxx';
  const actual = _layoutParserHelper(s).map(t => t.type);
  const expected = [TileTypes.Tile, TileTypes.Tile, TileTypes.Tile];
  expect(actual).toEqual(expected);
});

test('single line - remove first line', () => {
  const s = '    \nxxx';
  const actual = _layoutParserHelper(s).map(t => t.type);
  const expected = [TileTypes.Tile, TileTypes.Tile, TileTypes.Tile];
  expect(actual).toEqual(expected);
});

test('three lines single string', () => {
  const s = 'x\nxx\nx';
  const actual = _layoutParserHelper(s).map(t => t.type);
  const expected = [TileTypes.Tile, TileTypes.Newline, TileTypes.Tile, TileTypes.Tile, TileTypes.Newline, TileTypes.Tile];
  expect(actual).toEqual(expected);
});

test('three lines single string - remove first line', () => {
  const s = '     \nx\nxx\nx';
  const actual = _layoutParserHelper(s).map(t => t.type);
  const expected = [TileTypes.Tile, TileTypes.Newline, TileTypes.Tile, TileTypes.Tile, TileTypes.Newline, TileTypes.Tile];
  expect(actual).toEqual(expected);
});

test('three lines three string', () => {
  const s = ['x', 'xx', 'x'];
  const actual = _layoutParserHelper(s).map(t => t.type);
  const expected = [TileTypes.Tile, TileTypes.Newline, TileTypes.Tile, TileTypes.Tile, TileTypes.Newline, TileTypes.Tile];
  expect(actual).toEqual(expected);
});

test('three lines three string with a space', () => {
  const s = ['x', 'x x', 'x'];
  const actual = _layoutParserHelper(s).map(t => t.type);
  const expected = [TileTypes.Tile, TileTypes.Newline, TileTypes.Tile, TileTypes.Space, TileTypes.Tile, TileTypes.Newline, TileTypes.Tile];
  expect(actual).toEqual(expected);
});

test('half widths', () => {
  const s = 'x x,x';
  const actual = _layoutParserHelper(s).map(t => t.type);
  const expected = [TileTypes.Tile, TileTypes.Space, TileTypes.Tile, TileTypes.HalfSpace, TileTypes.Tile];
  expect(actual).toEqual(expected);
});
