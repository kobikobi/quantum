import { serializeGameToSimpleObject, deserializeSimpleObjectToGame } from '../services/GameSerializer';
import Game from '../QuantumGame';
import { ArrayBasedMock } from '../GameRandomPolicy';
import Picture from '../Picture';

/**
 * Take the game, serialize it, deserialize it, and compare every thing thing
 */
function compareEverything(game) {
  const serialized = serializeGameToSimpleObject(game);
  const loaded = deserializeSimpleObjectToGame(serialized);
  test('tileCount', () => expect(loaded.tileCount).toBe(game.tileCount));
  test('gameState', () => expect(loaded.gameState).toBe(game.gameState));

  test('turnCount', () => expect(loaded.turnCount).toBe(game.turnCount));
  test('pairsFoundCount', () => expect(loaded.pairsFoundCount).toBe(game.pairsFoundCount));
  test('gameMode', () => expect(loaded.gameMode).toBe(game.gameMode));
  test('gameStartTimestamp', () => expect(loaded.gameStartTimestamp).toEqual(game.gameStartTimestamp));

  test(
    'firstShowingTilePreviouslyKnown',
    () => expect(loaded.firstShowingTilePreviouslyKnown).toBe(game.firstShowingTilePreviouslyKnown),
  );
  test(
    'secondShowingTilePreviouslyKnown',
    () => expect(loaded.secondShowingTilePreviouslyKnown).toBe(game.secondShowingTilePreviouslyKnown),
  );
  test('board.drawBoard', () => expect(loaded.board.drawBoard()).toBe(game.board.drawBoard()));

  test('deep equal - firstShowingTile', () => expect(loaded.firstShowingTile).toEqual(game.firstShowingTile));
  test('deep equal - secondShowingTile', () => expect(loaded.secondShowingTile).toEqual(game.secondShowingTile));
  test('deep equal - lastTurnEndResult', () => expect(loaded.lastTurnEndResult).toEqual(game.lastTurnEndResult));

  test('pictures defined', () => expect(loaded.pictures).toBeDefined());
  test('pictures length', () => expect(loaded.pictures.length).toBe(game.pictures.length));
  test('pictures', () => expect(loaded.pictures).toEqual(game.pictures));
  return true;
}

describe('new game', () => {
  const game = new Game();
  compareEverything(game, 'new game');
});

// test('new game ALL OK', ()=>expect(result).toBeTruthy())

describe('pick one tile', () => {
  const game = new Game();
  game.pickTile(0);
  compareEverything(game);
});

describe('pick two tiles', () => {
  const game = new Game();
  game.pickTile(0);
  game.pickTile(1);
  compareEverything(game);
});

describe('pick two tiles and complete turn', () => {
  const game = new Game();
  game.pickTile(0);
  game.pickTile(1);
  game.completeTurn();
  compareEverything(game);
});

describe('complete turn - matching tiles', () => {
  const pictureOrder = [2, 2];
  const mockRandom = new ArrayBasedMock(pictureOrder);
  const game = new Game(mockRandom);
  game.pickTile(0);
  game.pickTile(1);
  game.completeTurn();
  compareEverything(game);
});

describe('new game - indexed pictures', () => {
  const pictures = [new Picture('aaa', 5), new Picture('bbbb', 1), new Picture('ccccc', 0)];
  const game = new Game(undefined, undefined, undefined, pictures);
  compareEverything(game, 'new game');
});

describe('complete turn - no match - indexed pictures', () => {
  const pictures = [new Picture('aaa', 5), new Picture('bbbb', 1), new Picture('ccccc', 0)];
  const pictureOrder = [0, 1];
  const mockRandom = new ArrayBasedMock(pictureOrder);
  const game = new Game(mockRandom, undefined, undefined, pictures);
  game.pickTile(0);
  game.pickTile(1);
  game.completeTurn();
  compareEverything(game);
});

function testFailCauseShouldHavePickedKnown(forthPick) {
  const pictureOrder = [1, 2, 1, 3];
  const mockRandom = new ArrayBasedMock(pictureOrder);
  const game = new Game(mockRandom);
  game.pickTile(1);
  game.pickTile(2);
  game.completeTurn();
  // now the player knows tiles 1 contains picture 1.
  // if the player sees 1 as first step in a new turn and doesn't pick tile 1 as matching - the game should fail.
  game.pickTile(3);
  game.pickTile(forthPick);
  game.completeTurn();
  return game;
}

describe(
  'complete turn - fail - should have picked known tile',
  () => { compareEverything(testFailCauseShouldHavePickedKnown(4)); },
);
describe(
  'complete turn - fail - should have picked known tile - conflict with already known tile',
  () => { compareEverything(testFailCauseShouldHavePickedKnown(2)); },
);

test(
  'complete turn - fail - should have picked known tile - TEST nested',
  () => {
    const game = testFailCauseShouldHavePickedKnown(2);
    const serialized = serializeGameToSimpleObject(game);
    const loaded = deserializeSimpleObjectToGame(serialized);
    // ignore randomPolicy
    game.randomPolicy = undefined;
    loaded.randomPolicy = undefined;
    expect(game).toEqual(loaded);
  },
);

test(
  'complete turn - fail - should have picked known tile - JSON serializer',
  () => {
    const game = testFailCauseShouldHavePickedKnown(2);
    let serialized = serializeGameToSimpleObject(game);
    // use JSON to ensure everything stays the same.
    serialized = JSON.parse(JSON.stringify(serialized));
    const loaded = deserializeSimpleObjectToGame(serialized);
    // ignore randomPolicy
    game.randomPolicy = undefined;
    loaded.randomPolicy = undefined;
    expect(game).toEqual(loaded);
  },
);
