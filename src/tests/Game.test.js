import Game from '../QuantumGame';
import { ArrayBasedMock } from '../GameRandomPolicy';
import { GameStates, TurnEndResults, FailureReasons } from '../Constants';
// import Tile from '../Tile';
// import Picture from '../Picture';

test('new game state', () => {
  const game = new Game();
  // expect(Tile.state).toBe(Tile);
  // todo: get TileStates
  expect(game.board.tiles.length).toBeGreaterThan(0);
});

test('pick two tiles', () => {
  const game = new Game();
  expect(game.gameState).toBe(GameStates.turnStart);
  game.pickTile(0);
  expect(game.gameState).toBe(GameStates.showingOneTile);
  game.pickTile(1);
  expect(game.gameState).toBe(GameStates.showingTwoTiles);
  game.completeTurn();
  expect(game.gameState).toBe(GameStates.turnStart);
});

test('pick same tile twice exception', () => {
  const game = new Game();
  game.pickTile(0);
  expect(() => game.pickTile(0)).toThrow();
});

test('pick undefined tile', () => {
  const game = new Game();
  expect(() => game.pickTile(100000)).toThrow();
});

test('set selected tiles', () => {
  const pictureOrder = [1, 2];
  const mockRandom = new ArrayBasedMock(pictureOrder);
  const game = new Game(mockRandom);
  game.pickTile(1);
  expect(game.firstShowingTile.picture).toBe(game.pictures[1]);
  expect(game.firstShowingTilePreviouslyKnown).toBe(false);
  game.pickTile(2);
  expect(game.secondShowingTile.picture).toBe(game.pictures[2]);
});

test('complete turn - reset selected tiles', () => {
  const pictureOrder = [1, 2];
  const mockRandom = new ArrayBasedMock(pictureOrder);
  const game = new Game(mockRandom);
  game.pickTile(1);
  game.pickTile(2);
  game.completeTurn();
  expect(game.firstShowingTile).toBeUndefined();
  expect(game.secondShowingTile).toBeUndefined();
  expect(game.firstShowingTilePreviouslyKnown).toBeUndefined();
  expect(game.secondShowingTilePreviouslyKnown).toBeUndefined();
});

test('complete turn - turn count - game data', () => {
  const pictureOrder = [1, 2];
  const mockRandom = new ArrayBasedMock(pictureOrder);
  const game = new Game(mockRandom);
  expect(game.turnCount).toBe(0);
  game.pickTile(0);
  game.pickTile(1);
  expect(game.turnCount).toBe(0);
  game.completeTurn();
  expect(game.turnCount).toBe(1);
});

test('complete turn - turn count - game data - two turns', () => {
  const pictureOrder = [1, 2, 3, 4];
  const mockRandom = new ArrayBasedMock(pictureOrder);
  const game = new Game(mockRandom);
  expect(game.turnCount).toBe(0);
  game.pickTile(0);
  game.pickTile(1);
  expect(game.turnCount).toBe(0);
  game.completeTurn();
  expect(game.turnCount).toBe(1);
  game.pickTile(2);
  game.pickTile(3);
  expect(game.turnCount).toBe(1);
  game.completeTurn();
  expect(game.turnCount).toBe(2);
});

test('complete turn - turn count - turn end data does not change after turn', () => {
  const pictureOrder = [1, 2];
  const mockRandom = new ArrayBasedMock(pictureOrder);
  const game = new Game(mockRandom);
  game.pickTile(0);
  const turnEndData = game.pickTile(1);
  expect(turnEndData.turnEndResult.turnCount).toBe(0);
  game.completeTurn();
  expect(turnEndData.turnEndResult.turnCount).toBe(0);
});

test('complete turn - turn count - turn end data does not change after turn - two turns', () => {
  const pictureOrder = [1, 2, 3, 4];
  const mockRandom = new ArrayBasedMock(pictureOrder);
  const game = new Game(mockRandom);
  game.pickTile(0);
  const firstTurnEndData = game.pickTile(1);
  expect(firstTurnEndData.turnEndResult.turnCount).toBe(0);
  game.completeTurn();
  expect(firstTurnEndData.turnEndResult.turnCount).toBe(0);
  game.pickTile(2);
  const secondTurnEndData = game.pickTile(3);
  expect(secondTurnEndData.turnEndResult.turnCount).toBe(1);
  game.completeTurn();
  expect(secondTurnEndData.turnEndResult.turnCount).toBe(1);
  expect(firstTurnEndData.turnEndResult.turnCount).toBe(0);
});

test('complete turn - non matching tiles', () => {
  const pictureOrder = [1, 2];
  const mockRandom = new ArrayBasedMock(pictureOrder);
  const game = new Game(mockRandom);
  game.pickTile(1);
  game.pickTile(2);
  const endTurnData = game.completeTurnState();
  expect(endTurnData.result).toBe(TurnEndResults.RevealTwoTiles);
});

test('complete turn - matching tiles', () => {
  const pictureOrder = [2, 2];
  const mockRandom = new ArrayBasedMock(pictureOrder);
  const game = new Game(mockRandom);
  game.pickTile(1);
  const endTurnData = game.pickTile(2).turnEndResult;
  expect(endTurnData.result).toBe(TurnEndResults.FoundPair);
});

function testFailCause(fifthPick, sixthPick, expectedFailedTile) {
  const pictureOrder = [1, 2, 3, 1, 4, 5];
  const mockRandom = new ArrayBasedMock(pictureOrder);
  const game = new Game(mockRandom);
  game.pickTile(1);
  game.pickTile(2);
  game.completeTurn();
  game.pickTile(3);
  game.pickTile(4);
  game.completeTurn();
  // now the player knows tiles 1 and 4 are a matching.
  // player picks 1 or 2 with a different tile - should fail.
  game.pickTile(fifthPick);
  const endTurnData = game.pickTile(sixthPick).turnEndResult;
  expect(endTurnData.result).toBe(TurnEndResults.Fail);
  expect(endTurnData.failReason).toBe(FailureReasons.TileSeenSecondTimeWithoutMatch);
  expect(endTurnData.failCauseTile).toBe(game.board.tiles[expectedFailedTile]);
}

// two tests are commented out because FailedToSelecthKnwonSecondTile has precedence over these cases.

// test('complete turn - fail - tile visible second time 1', () => testFailCause(1,5,1));
test('complete turn - fail - tile visible second time 2', () => testFailCause(5, 1, 1));
// test('complete turn - fail - tile visible second time 3', () => testFailCause(4,5,4));
test('complete turn - fail - tile visible second time 4', () => testFailCause(5, 4, 4));

function testFailCauseShouldHavePickedKnown(forthPick) {
  const pictureOrder = [1, 2, 1, 3];
  const mockRandom = new ArrayBasedMock(pictureOrder);
  const game = new Game(mockRandom);
  game.pickTile(1);
  game.pickTile(2);
  game.completeTurn();
  // now the player knows tiles 1 contains picture 1.
  // if the player sees 1 as first step in a new turn and doesn't pick tile 1 as matching - the game should fail.
  game.pickTile(3);
  const endTurnData = game.pickTile(forthPick).turnEndResult;
  game.completeTurn();
  expect(endTurnData.result).toBe(TurnEndResults.Fail);
  expect(endTurnData.failReason).toBe(FailureReasons.FailedToSelecthKnwonSecondTile);
  expect(endTurnData.failCauseTile).toBe(game.board.tiles[1]);
  expect(game.gameState).toBe(GameStates.failed);
}

test(
  'complete turn - fail - should have picked known tile',
  () => testFailCauseShouldHavePickedKnown(4),
);
test(
  'complete turn - fail - should have picked known tile - conflict with already known tile',
  () => testFailCauseShouldHavePickedKnown(2),
);

for (let times = 0; times < 50; times++) {
  test(`simulate game ${times}`, () => {
    const game = new Game();
    for (let i = 0; i < game.board.tiles.length; i++) {
      game.pickTile(i);
      if (i % 2 === 1) {
        game.completeTurn();
      }
      if (game.gameState === GameStates.failed) {
        break;
      }
    }
  });
}
