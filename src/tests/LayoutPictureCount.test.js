import { Layouts } from '../components/Layouts';
import { Tilesets, TilesetNames } from '../components/Tilesets';

const tilesetData = TilesetNames.map(tileset => ({ name: tileset, pictureCount: Tilesets[tileset].Glyphs.length }));
const minGlyphsPerTileset = Math.min.apply(null, tilesetData.map(d => d.pictureCount));

/* eslint-disable no-loop-func */

// test that each layout cannot get to an unsolvable state.
let allLayoutsAreSolvable = true;
for (const [layoutName, layout] of Layouts) {
  test(`layout cannot get to unsolvable state ${layoutName}`, () => {
    const layoutConfiguredPictureCount = layout.pictureCount;
    const layoutTileCount = layout.tileCount;
    allLayoutsAreSolvable = allLayoutsAreSolvable && (layoutConfiguredPictureCount < layoutTileCount);
    // assert - if a layout has 4 tiles, it can has as many as 3 pictures, so one pair is always available.
    expect(layoutConfiguredPictureCount).toBeLessThan(layoutTileCount);
  });
}

test('all layout cannot get to unsolvable state', () => {
  expect(allLayoutsAreSolvable).toBeTruthy();
});

// test that each layout is configured to have enough pictures to cover it.
let allLayoutsHaveEnoughPictures = true;
for (const [layoutName, layout] of Layouts) {
  test(`layout has enough pictures to cover it ${layoutName}`, () => {
    const layoutConfiguredPictureCount = layout.pictureCount;
    const layoutTileCount = layout.tileCount;
    allLayoutsHaveEnoughPictures = allLayoutsHaveEnoughPictures && (layoutTileCount <= layoutConfiguredPictureCount * 2);
    // assert - if we click on all tiles, we will always have enough pictures.
    expect(layoutTileCount).toBeLessThanOrEqual(layoutConfiguredPictureCount * 2);
  });
}

test('all layout have enough pictures', () => {
  expect(allLayoutsHaveEnoughPictures).toBeTruthy();
});

// test that layouts do not require more pictures than we have.
let allLayoutsPictureCountIsOk = true;
for (const [layoutName, layout] of Layouts) {
  test(`layout ${layoutName} PictureCount is less than glyph count`, () => {
    const layoutConfiguredPictureCount = layout.pictureCount;
    allLayoutsPictureCountIsOk = allLayoutsPictureCountIsOk && (layoutConfiguredPictureCount <= minGlyphsPerTileset);
    // assert - if we click on all tiles, we will always have enough pictures.
    expect(layoutConfiguredPictureCount).toBeLessThanOrEqual(minGlyphsPerTileset);
  });
}

test('all layouts PictureCount is less than glyph count', () => {
  expect(allLayoutsPictureCountIsOk).toBeTruthy();
});
