import Game from '../QuantumGame';
import { ArrayBasedMock } from '../GameRandomPolicy';
import {
  GameStates, TurnEndResults, GameModes,
} from '../Constants';

function testFailCause(fifthPick, sixthPick) {
  const pictureOrder = [1, 2, 3, 1, 4, 5];
  const mockRandom = new ArrayBasedMock(pictureOrder);
  const game = new Game(mockRandom, GameModes.Casual);
  game.pickTile(1);
  game.pickTile(2);
  game.completeTurn();
  game.pickTile(3);
  game.pickTile(4);
  game.completeTurn();
  // now the player knows tiles 1 and 4 are a matching.
  // player picks 1 or 2 with a different tile - should fail (but this is a casual game).
  game.pickTile(fifthPick);
  const endTurnData = game.pickTile(sixthPick).turnEndResult;
  expect(endTurnData.result).toBe(TurnEndResults.RevealTwoTiles);
  expect(endTurnData.failReason).toBe(undefined);
  expect(endTurnData.failCauseTile).toBe(undefined);
}

test('casual - complete turn - no fail - tile visible second time 1', () => testFailCause(1, 5));
test('casual - complete turn - no fail - tile visible second time 2', () => testFailCause(5, 1));
test('casual - complete turn - no fail - tile visible second time 3', () => testFailCause(4, 5));
test('casual - complete turn - no fail - tile visible second time 4', () => testFailCause(5, 4));

function testFailCauseShouldHavePickedKnown(forthPick) {
  const pictureOrder = [1, 2, 1, 3];
  const mockRandom = new ArrayBasedMock(pictureOrder);
  const game = new Game(mockRandom, GameModes.Casual);
  game.pickTile(1);
  game.pickTile(2);
  game.completeTurn();
  // now the player knows tiles 1 and 2 are a matching.
  // player picks 1 or 2 with a different tile - should fail (but this is a casual game).
  game.pickTile(3);
  const endTurnData = game.pickTile(forthPick).turnEndResult;
  game.completeTurn();
  expect(endTurnData.result).toBe(TurnEndResults.RevealTwoTiles);
  expect(endTurnData.failReason).toBe(undefined);
  expect(endTurnData.failCauseTile).toBe(undefined);
  expect(game.gameState).toBe(GameStates.turnStart);
}

test('casual complete turn - no fail - should', () => testFailCauseShouldHavePickedKnown(4));

for (let times = 0; times < 50; times++) {
  test(`simulate casual game ${times}`, () => {
    const game = new Game(undefined, GameModes.Casual);
    for (let i = 0; i < game.board.tiles.length; i++) {
      game.pickTile(i);
      if (i % 2 === 1) {
        game.completeTurn();
      }
      if (game.gameState === GameStates.failed) {
        throw new Error('Should never be on failed state on Casual mode.');
      }
    }
  });
}
