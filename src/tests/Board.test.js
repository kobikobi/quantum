import Tile from '../Tile';
import Picture from '../Picture';
import Board from '../Board';

const tileCount = 10;

function createBoard() {
  const tiles = new Array(10).fill().map(() => new Tile());
  return new Board(tiles);
}

test('board tile length', () => {
  const board = createBoard();
  expect(board.tiles.length).toBe(tileCount);
});

test('getKnownTiles()', () => {
  const board = createBoard();
  expect(board.getKnownTiles()).toHaveLength(0);
  board.tiles[1].reveal(new Picture('x'));
  expect(board.getKnownTiles()).toHaveLength(1);
  board.tiles[2].reveal(new Picture('y'));
  expect(board.getKnownTiles()).toHaveLength(2);
  board.tiles[2].clear();
  expect(board.getKnownTiles()).toHaveLength(1);
});

test('getKnownTiles(picture)', () => {
  const board = createBoard();
  expect(board.getKnownTiles()).toHaveLength(0);
  const pic = new Picture('x');
  board.tiles[1].reveal(pic);
  expect(board.getKnownTiles(pic)).toHaveLength(1);
  board.tiles[2].reveal(new Picture('y'));
  expect(board.getKnownTiles(pic)).toHaveLength(1);
  board.tiles[1].clear();
  expect(board.getKnownTiles(pic)).toHaveLength(0);
});
