import Tile from '../Tile';
import Picture from '../Picture';
import { TileStates } from '../Constants';

test('new tile state', () => {
  const tile = new Tile();
  // expect(Tile.state).toBe(Tile);
  // todo: get TileStates
  expect(tile.picture).toBe(undefined);
  expect(tile.state).toBe(TileStates.unknown);
});

test('set picture', () => {
  const picture = new Picture('Hello');
  const tile = new Tile();
  tile.reveal(picture);
  expect(tile.picture).toBe(picture);
  expect(tile.picture.name).toBe(picture.name);
  expect(tile.state).toBe(TileStates.revealed);
});

test('set picture multiple times throws', () => {
  const picture = new Picture('Hello');
  const tile = new Tile();
  tile.reveal(picture);
  expect(() => tile.reveal(picture)).toThrow();
});
