// this file is needed because after I upgraded Jest I had four modules fail with
// ` ReferenceError: regeneratorRuntime is not defined`
// see also: https://stackoverflow.com/a/57439821/7586
import 'regenerator-runtime/runtime';
