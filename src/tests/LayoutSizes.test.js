import { _layoutParserHelper } from '../components/Layouts';

test('auto layout height 1', () => {
  const s = ['x'];
  const actual = _layoutParserHelper(s);
  expect(actual.tileHeight).toEqual(1);
});
test('auto layout height 2', () => {
  const s = ['x', 'xx'];
  const actual = _layoutParserHelper(s);
  expect(actual.tileHeight).toEqual(2);
});
test('auto layout height 3', () => {
  const s = ['x', 'xx', 'x'];
  const actual = _layoutParserHelper(s);
  expect(actual.tileHeight).toEqual(3);
});

test('auto layout width simple', () => {
  const s = ['x', 'xx', 'x'];
  const actual = _layoutParserHelper(s);

  expect(actual.tileWidth).toEqual(2);
  expect(actual.tileHeight).toEqual(3);
});

test('auto layout width simple space', () => {
  const s = ['x', 'x x', 'x'];
  const actual = _layoutParserHelper(s);

  expect(actual.tileWidth).toEqual(3);
  expect(actual.tileHeight).toEqual(3);
});

test('auto layout width simple half space', () => {
  const s = ['x', 'x,x', 'x'];
  const actual = _layoutParserHelper(s);

  expect(actual.tileWidth).toEqual(3);
  expect(actual.tileHeight).toEqual(3);
});

test('auto layout width simple two halfs', () => {
  const s = ['x', 'x,,x', 'x'];
  const actual = _layoutParserHelper(s);

  expect(actual.tileWidth).toEqual(3);
  expect(actual.tileHeight).toEqual(3);
});

test('auto layout width simple tow halfs and space', () => {
  const s = ['x', 'x, ,x', 'x'];
  const actual = _layoutParserHelper(s);

  expect(actual.tileWidth).toEqual(4);
  expect(actual.tileHeight).toEqual(3);
});
