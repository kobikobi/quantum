const turnEndResults = Object.freeze({
  FoundPair: 'Found a pair',
  RevealTwoTiles: 'Revealed two unknown tiles',
  Fail: 'Fail',
});

const failureReasons = Object.freeze({
  /**
     * After seeing the first tile, the matching tile is already known and should have
     * been selected.
     */
  FailedToSelecthKnwonSecondTile: 'known matching card should have been the second choice',
  /**
     * A tile can we flipped only once without being matched.
     */
  TileSeenSecondTimeWithoutMatch: 'Card flipped twice without match',
});

const gameStates = Object.freeze({
  turnStart: 'Turn Start',
  showingOneTile: 'One card showing',
  showingTwoTiles: 'Two cards showing',
  failed: ':-(',
});

const tileStates = Object.freeze({ unknown: 'Unknown', revealed: 'Revealed' });

const gameModes = Object.freeze({ Brutal: 'Brutal', Casual: 'Casual' });

module.exports = {
  TurnEndResults: turnEndResults,
  FailureReasons: failureReasons,
  GameStates: gameStates,
  TileStates: tileStates,
  GameModes: gameModes,
};
