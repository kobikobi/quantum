import Picture from './Picture';
import Tile from './Tile';
import Board from './Board';
import {
  GameStates, TileStates, TurnEndResults, FailureReasons, GameModes,
} from './Constants';
import { DefaultPolicy as defaultGameRandomPolicy } from './GameRandomPolicy';

class Game {
  constructor(randomPolicy, gameMode, tileCount, pictures) {
    this.pictures = pictures || ['apple', 'banana', 'dog', 'bird', 'dinosaur', 'whale', 'monkey', 'rhinoceros']
      .map((n, i) => new Picture(n, i));
    this.tileCount = tileCount || 12;
    this.randomPolicy = randomPolicy || defaultGameRandomPolicy;
    this.board = new Board(new Array(this.tileCount).fill().map(() => new Tile()));
    this.gameState = GameStates.turnStart;
    this.firstShowingTile = undefined;
    this.secondShowingTile = undefined;
    this.turnCount = 0;
    this.pairsFoundCount = 0;
    this.gameMode = gameMode || GameModes.Brutal;
    this.gameStartTimestamp = new Date();
  }

  selectRandomPictureForNextTile() {
    return this.randomPolicy.selectPicture(this.pictures, this.board);
  }

  pickTile(tile) {
    if (typeof tile === 'number') {
      // eslint-disable-next-line no-param-reassign
      tile = this.board.tiles[tile];
    }
    if (tile instanceof Tile === false) {
      throw new Error('Argument should either be Tile or index of a tile.');
    }
    switch (this.gameState) {
      case (GameStates.turnStart):
        this.firstShowingTilePreviouslyKnown = tile.state === TileStates.revealed;
        if (!this.firstShowingTilePreviouslyKnown) {
          const picture = this.selectRandomPictureForNextTile();
          tile.reveal(picture);
        }
        this.firstShowingTile = tile;
        this.gameState = GameStates.showingOneTile;
        return undefined; // todo: return something better
      case (GameStates.showingOneTile):
        if (tile === this.firstShowingTile) {
          throw new Error('You cannot select the same tile twice in the same turn.');
        }
        this.secondShowingTilePreviouslyKnown = tile.state === TileStates.revealed;
        if (!this.secondShowingTilePreviouslyKnown) {
          const picture = this.selectRandomPictureForNextTile();
          tile.reveal(picture);
        }
        this.secondShowingTile = tile;
        this.gameState = GameStates.showingTwoTiles;
        this.lastTurnEndResult = this.completeTurnState();
        if (this.lastTurnEndResult.result === TurnEndResults.Fail) {
          this.gameState = GameStates.failed;
        } else {
          this.gameState = GameStates.showingTwoTiles;
        }
        return { turnEndResult: this.lastTurnEndResult };
      default:
        throw new Error(`The game state '${this.gameState}' is not implemented yet.`);
    }
  }

  /**
     * Complete the turn after selecting two tiles.
     */
  completeTurnState() {
    // same picture in both tiles - success.
    if (this.firstShowingTile.picture === this.secondShowingTile.picture) {
      return {
        result: TurnEndResults.FoundPair,
        firstShowingTile: this.firstShowingTile,
        secondShowingTile: this.secondShowingTile,
        turnCount: this.turnCount,
      };
    }

    if (this.gameMode === GameModes.Brutal) {
      // first revealed, matching should have been selected as second but wasn't.
      // this condition is checked before FailureReasons.TileSeenSecondTimeWithoutMatch becuae
      // it is the more interesting failure reason, and should be displayed to the user.
      const firstSelectedPicture = this.firstShowingTile.picture;
      const knownMatchingTile = this.board.getKnownTiles(firstSelectedPicture)
        .filter(t => t !== this.firstShowingTile);
      if (knownMatchingTile.length === 2) {
        throw new Error('invalid state - cannot have two matching tiles to the first selected tile.');
      } else if (knownMatchingTile.length === 1) {
        return {
          result: TurnEndResults.Fail,
          failReason: FailureReasons.FailedToSelecthKnwonSecondTile,
          failCauseTile: knownMatchingTile[0],
          firstShowingTile: this.firstShowingTile,
          secondShowingTile: this.secondShowingTile,
          turnCount: this.turnCount,
        };
      }

      // first or second tiles are already revealed.
      if (this.firstShowingTilePreviouslyKnown) {
        return {
          result: TurnEndResults.Fail,
          failReason: FailureReasons.TileSeenSecondTimeWithoutMatch,
          failCauseTile: this.firstShowingTile,
          firstShowingTile: this.firstShowingTile,
          secondShowingTile: this.secondShowingTile,
          turnCount: this.turnCount,
        };
      }
      if (this.secondShowingTilePreviouslyKnown) {
        return {
          result: TurnEndResults.Fail,
          failReason: FailureReasons.TileSeenSecondTimeWithoutMatch,
          failCauseTile: this.secondShowingTile,
          firstShowingTile: this.firstShowingTile,
          secondShowingTile: this.secondShowingTile,
          turnCount: this.turnCount,
        };
      }
    }

    // two new unknowns, first one without potential match.
    return {
      result: TurnEndResults.RevealTwoTiles,
      firstShowingTile: this.firstShowingTile,
      secondShowingTile: this.secondShowingTile,
      turnCount: this.turnCount,
    };
  }

  completeTurn() {
    // fail quietly if callled during a turn.
    if (!this.lastTurnEndResult
            || this.gameState === GameStates.turnStart || this.gameState === GameStates.showingOneTile) {
      return;
    }
    this.turnCount++;
    if (this.lastTurnEndResult.result === TurnEndResults.FoundPair) {
      this.foundPair();
    }
    if (this.gameState === GameStates.showingTwoTiles) {
      this.gameState = GameStates.turnStart;
    }
    this.clearState();
  }

  clearState() {
    this.firstShowingTile = undefined;
    this.firstShowingTilePreviouslyKnown = undefined;
    this.secondShowingTile = undefined;
    this.secondShowingTilePreviouslyKnown = undefined;
  }

  foundPair() {
    this.firstShowingTile.clear();
    this.secondShowingTile.clear();
    this.pairsFoundCount++;
  }
}

export default Game;
