import { TileStates } from './Constants';

class Tile {
  constructor() {
    this.picture = undefined;
    this.state = TileStates.unknown;
  }

  reveal(picture) {
    if (this.state === TileStates.revealed) {
      throw new Error('this tile is already revealed, so its picture cannot be set.');
    }
    if (picture === undefined) {
      throw new Error('cannot reveal tile with an undefined picture');
    }
    this.picture = picture;
    this.state = TileStates.revealed;
  }

  clear() {
    this.picture = undefined;
    this.state = TileStates.unknown;
  }

  toString() {
    let s = `[state:${this.state}]`;
    if (this.picture) {
      s += ` ${this.picture.name}`;
    }
    return s;
  }
}

export default Tile;
