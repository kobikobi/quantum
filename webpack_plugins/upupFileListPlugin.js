// based on https://v4.webpack.js.org/contribute/writing-a-plugin/#example
// UpUp is a small service worker that writes files to local cache. This plugin helps get the list of files.

class UpUpFileListPlugin {
  constructor({
    test = /./,
    targetFile = 'index.html',
    replacePlaceholder = '/*~~UpUpStaticAssetsList~~*/',
    writeLeadingComma = true,
    writeTrailingComma = false,
  }) {
    this.test = test;
    this.targetFile = targetFile;
    this.replacePlaceholder = replacePlaceholder;
    this.writeLeadingComma = writeLeadingComma;
    this.writeTrailingComma = writeTrailingComma;
  }

  apply(compiler) {
    // emit is asynchronous hook, tapping into it using tapAsync, you can use tapPromise/tap(synchronous) as well
    compiler.hooks.emit.tapAsync('UpUpFileListPlugin', (compilation, callback) => {
      // compilation.assets isn't a real array, it doesn't have .filter.
      const includedStaticFiles = [];
      for (const filename in compilation.assets) {
        if (filename.match(this.test)) {
          includedStaticFiles.push(filename);
        }
      }

      compilation.updateAsset(this.targetFile, file => {
        if (includedStaticFiles.length === 0) {
          return file;
        }
        const fileContent = file.source();
        const staticFileList = JSON.stringify(includedStaticFiles).replace(/^\[|\]$/g, '');
        const newFileContent = fileContent.replace(
          this.replacePlaceholder,
          (this.writeLeadingComma ? ',' : '') + staticFileList + (this.writeTrailingComma ? ',' : ''),
        );
        return {
          source: () => newFileContent,
          size: () => newFileContent.length,
        };
      });

      callback();
    });
  }
}

module.exports = UpUpFileListPlugin;
