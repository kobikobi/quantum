// boilerplate from https://github.com/marharyta/webpack-boilerplate/tree/master/webpack-basic-setup - thanks!
// webpack v4
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const WebpackMd5Hash = require('webpack-md5-hash');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
// const CleanWebpackPlugin = require('clean-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
// const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const UpUpFileListPlugin = require('./webpack_plugins/upupFileListPlugin')

module.exports = {
  entry: {
    quantum: './src/components/index.js', // "./src/QuantumGame.js",
    style: './src/style/DummyCssImporter.js',
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].js',
  },
  devServer: {
    contentBase: './dist',
    port: 7700,
  },
  optimization: {
    sideEffects: false,
    splitChunks: {
      chunks: 'all',
      automaticNameDelimiter: '~',
      name: true,
      cacheGroups: {
        vendors: {
          test: /[\\/]node_modules[\\/]/,
          priority: -10,
        },
        svg: {
          test: /[\\/]images[\\/].*\.svg/,
          priority: -10,
        },
        default: {
          minChunks: 2,
          priority: -20,
          reuseExistingChunk: true,
        },
      },
    },
  },
  module: {
    rules: [
      {
        test: /\.js(?:x)?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          // options: {
          //  presets: ['react']
          // },
        },
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          MiniCssExtractPlugin.loader,
          'css-loader',
          'postcss-loader',
        ],
      },
      {
        test: /\.scss$/,
        use: [
          'style-loader', // creates style nodes from JS strings
          MiniCssExtractPlugin.loader,
          'css-loader', // translates CSS into CommonJS
          'sass-loader', // compiles Sass to CSS, using Node Sass by default
        ],
      },
      {
        test: /\.(gif|png|jpe?g)$/i,
        use: [
          'file-loader',
          {
            loader: 'image-webpack-loader',
            options: {
              bypassOnDebug: true,
            },
          },
        ],
      },
      // source: https://www.npmjs.com/package/react-svg-loader
      // use as
      // import Image1 from './image1.svg';
      // <Image1 width={50} height={50}/>
      {
        test: /\.svg$/,
        exclude: path.resolve(__dirname, 'src/images/tiles'),
        use: [
          'babel-loader',
          {
            loader: 'react-svg-loader',
            options: {
              svgo: {
                plugins: [
                  { removeTitle: false },
                ],
                floatPrecision: 2,
              },
            },
          },
        ],
      },
      // SVGs used as tiles, not as icons.
      // it is more convenient not to use each one as a component, but as data parameters of the same component.
      {
        test: /\.svg$/,
        include: path.resolve(__dirname, 'src/images/tiles'),
        loader: 'svg-inline-loader',
      },
      {
        test: /\.(woff(2)?|ttf|eot|otf)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'fonts/',
            },
          },
        ],
      },
    ],
  },
  plugins: [
    // todo: CleanWebpackPlugin deleted dist when I run `yarn server`.
    // new CleanWebpackPlugin("dist", {}),
    new MiniCssExtractPlugin({
      filename: 'style.css',
    }),
    new HtmlWebpackPlugin({
      inject: false,
      template: './src/static/index.ejs',
      filename: 'index.html',
    }),
    new WebpackMd5Hash(),
    // new BundleAnalyzerPlugin(),//uncomment to get a popup with all pakages.
    new CopyWebpackPlugin([{
      from: 'assets/icon/!(*.html|*.md)', to: '.', flatten: true, test: /(?!.*\.(?:html|md))/i,
    }, {
      from: 'assets/upup/*.js', to: '.', flatten: true,
    }], {}),
    new UpUpFileListPlugin({
      test: /\.(?:png|ico|json)$/ig,
    }),
    // UpUpFileListPlugin updated index.html, so CompressionPlugin should come later to include these changes in index.html.gz file.
    new CompressionPlugin(),
  ],
  serve: {
    // content: path.resolve(__dirname, "dist")
    // openPath: '/hello/world',
    // publicPath: '/dist/'
  },
};
