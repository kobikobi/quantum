About
--

Quantum is an infinite, brutal memory game.  
Play at <https://kobikobi.gitlab.io/quantum/>

Build and Test
---
* `npm ci` - restore all packages.  
* `npm run test` - run all Jest tests.  
* `npm run build` - compile all code and pack to `/dist`.  
* `npm run dev` - combile a debug build to `/dist`, run a dev server at `http://localhost:8081`, and watch for changes using Nodemon.
* `./startLocalhost8000.sh` - script to run a static Python web server on `/dist`. This server is accessible by other local area network comuters and phones.
* `npm run lint`, `npm run lint:fix` - validate and fix code formatting.  

Tools
---
* **Frontend libraries**: React, Material UI.
* **Development tools**: Node, webpack, babel, npm, sass, Jest, GitLab, eslint.